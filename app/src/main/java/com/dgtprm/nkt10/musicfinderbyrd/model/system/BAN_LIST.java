package com.dgtprm.nkt10.musicfinderbyrd.model.system;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BAN_LIST implements Parcelable {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("response")
    @Expose
    private Response response;

    protected BAN_LIST(Parcel in) {
        response = in.readParcelable(Response.class.getClassLoader());
    }

    public static final Creator<BAN_LIST> CREATOR = new Creator<BAN_LIST>() {
        @Override
        public BAN_LIST createFromParcel(Parcel in) {
            return new BAN_LIST(in);
        }

        @Override
        public BAN_LIST[] newArray(int size) {
            return new BAN_LIST[size];
        }
    };

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(response, flags);
    }

    public static class Response implements Parcelable {
        @SerializedName("ids")
        @Expose
        private List<Integer> ids = new ArrayList<>();
        @SerializedName("queries")
        @Expose
        private List<String> queries = new ArrayList<>();
        @SerializedName("remove")
        @Expose
        private Object remove;


        protected Response(Parcel in) {
            queries = in.createStringArrayList();
        }

        public static final Creator<Response> CREATOR = new Creator<Response>() {
            @Override
            public Response createFromParcel(Parcel in) {
                return new Response(in);
            }

            @Override
            public Response[] newArray(int size) {
                return new Response[size];
            }
        };

        public List<Integer> getIds() {
            return ids;
        }

        public void setIds(List<Integer> ids) {
            this.ids = ids;
        }

        public List<String> getQueries() {
            return queries;
        }

        public void setQueries(List<String> queries) {
            this.queries = queries;
        }

        public Object getRemove() {
            return remove;
        }

        public void setRemove(Object remove) {
            this.remove = remove;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeStringList(queries);
        }
    }}
