package com.dgtprm.nkt10.musicfinderbyrd;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.model.GlobalUser;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity{

    private static final String TAG = App.TAG + "." + LoginActivity.class.getSimpleName();

    public static final String EXTRA_USER = "EXTRA_USER";

    private ProgressBar progressBar;

    private static final String[] myScopes = new String[]{VKScope.GROUPS, VKScope.FRIENDS, VKScope.AUDIO, VKScope.NOHTTPS, VKScope.WALL};

    private SharedPreferences mPref;

    private Tracker mTracker;

    private VKRequest.VKRequestListener mListener = new VKRequest.VKRequestListener() {
        @Override
        public void onComplete(VKResponse response) {
            Log.d(TAG, "onComplete: ");

            String responseString = GlobalUser.parseUser(response.responseString);
            GlobalUser user = new Gson().fromJson(responseString, GlobalUser.class);

            redirectToMain(user);
            super.onComplete(response);
        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            Log.d(TAG, "attemptFailed: ");
            super.attemptFailed(request, attemptNumber, totalAttempts);
        }

        @Override
        public void onError(VKError error) {
            Log.d(TAG, "onError: ");

            super.onError(error);
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            Log.d(TAG, "onProgress: ");
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");
        mPref = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_login);

        App app = (App) getApplication();
        mTracker = app.getDefaultTracker();

        progressBar = (ProgressBar) findViewById(R.id.login_progressBar);
    }

    public void onClick(View view){

        VKSdk.login(this, myScopes);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
// Пользователь успешно авторизовался
                turnOnProgress();
                VKApi.users().get(VKParameters.from("fields", "photo_100")).executeWithListener(mListener);
            }
            @Override
            public void onError(VKError error) {
// Произошла ошибка авторизации (например, пользователь запретил авторизацию)
                setLoggedIn(false);
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setLoggedIn(boolean what){

        String res = what ? getString(R.string.vksdk_login_success) : getString(R.string.vksdk_login_fail);

        mPref.edit().putBoolean(getString(R.string.access_token_key), what).apply();
        Toast.makeText(this, res, Toast.LENGTH_SHORT).show();
    }

    private void redirectToMain(GlobalUser user){

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(EXTRA_USER, user);
        saveData(user.getId(), user.getFirstName(), user.getLastName(), user.getPhoto100());
        setLoggedIn(true);
        startActivity(intent);
        this.finish();
    }

    private void saveData(long id, String firstName, String lastName, String photoUrl){

        mPref.edit().putString(getString(R.string.pref_firstName), firstName).apply();
        mPref.edit().putString(getString(R.string.pref_lastName), lastName).apply();
        mPref.edit().putString(getString(R.string.pref_avatar_url), photoUrl).apply();
        mPref.edit().putLong(getString(R.string.pref_userId), id).apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName("Login");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void turnOnProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void turnOffProgress(){
        progressBar.setVisibility(View.GONE);
    }
}

