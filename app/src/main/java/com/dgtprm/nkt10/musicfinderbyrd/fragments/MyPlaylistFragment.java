package com.dgtprm.nkt10.musicfinderbyrd.fragments;

import android.os.Bundle;

import com.dgtprm.nkt10.musicfinderbyrd.adapters.MyPlaylistAdapter;
import com.dgtprm.nkt10.musicfinderbyrd.model.PlaylistItem;

import java.util.ArrayList;

public class MyPlaylistFragment extends BasePlaylistFragment {

    private static final String EXTRA_PLAYLIST = "EXTRA_PLAYLIST";

    private PlaylistItem mPlaylist;


    public static MyPlaylistFragment newInstance(PlaylistItem playlistItem) {

        Bundle args = new Bundle();

        args.putParcelable(EXTRA_PLAYLIST, playlistItem);

        MyPlaylistFragment fragment = new MyPlaylistFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null){
            mPlaylist = args.getParcelable(EXTRA_PLAYLIST);
            if (mPlaylist != null)
                myData = new ArrayList<>(mPlaylist.getMusicList());
            else
                myData = new ArrayList<>();

            if (myData.size() > 0) {
                checkForDownloading(myData);
                checkForDownloaded(myData);
            }
        }
    }

    @Override
    public void setmAdapter() {
        recyclerView.setAdapter(new MyPlaylistAdapter(myData, mListener, mPlaylist));
    }

}
