package com.dgtprm.nkt10.musicfinderbyrd.adapters;

import android.util.Log;
import android.view.View;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.BasePlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicItemStateHolder;
import com.dgtprm.nkt10.musicfinderbyrd.sql.PlaylistInfoProvider;
import com.dgtprm.nkt10.musicfinderbyrd.util.LoadingEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlayerEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

public class DownloadedPlaylistAdapter extends BaseRecyclerViewAdapter {

    private static final String TAG = App.TAG + "." + DownloadedPlaylistAdapter.class.getSimpleName();

    public DownloadedPlaylistAdapter(List<MusicData.Item> items, BasePlaylistFragment.PlaylistFragmentInteractionListener listener) {
        super(items, listener);

        Log.d(TAG, "update: mValues.size() = " + mValues.size());
        Log.d(TAG, "update: downloaded size = " + DownloadedListHolder.getInstance().getDownloadedList().size());
        Log.d(TAG, "update: downloading size = " + DownloadingListHolder.getInstance().getDownloadingList().size());
    }

    @Override
    protected void subscribeOnObservables()  {
        if (PlaylistHolder.getInstance() == null) {
            PlaylistHolder.newInstance(new ArrayList<MusicData.Item>());
        }
        PlaylistHolder.getInstance().addObserver(this);
        DownloadingListHolder.getInstance().addObserver(this);
        DownloadedListHolder.getInstance().addObserver(this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        Log.d(TAG, "onBindViewHolder() called with: " + "holder = [" + holder + "], position = [" + position + "]");

        holder.progressBar.setVisibility(holder.mItem.getStateHolder().getProgressBarVisible() == View.VISIBLE ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void bindPlayButton(ViewHolder holder) {
        holder.playStopImage.setImageResource(holder.mItem.equals(PlaylistHolder.getInstance().getItemPlaying())
                ? R.drawable.ic_pause_ : R.drawable.bt_track_play);
        holder.orangeLine.setVisibility(holder.mItem.equals(PlaylistHolder.getInstance().getItemPlaying())
                ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    protected void setListeners(final ViewHolder holder) {
        holder.extraButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ");
                if (null != mListener) {
                    mListener.onExtraButtonPressed(holder);

                }
            }
        });

        holder.playButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mListener) {
                    mListener.onPlayButtonPressed(holder.mItem, mValues, false);

                }
            }
        });

        holder.loadingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mListener){
                    mListener.onDownloadRemoveButtonPressed(holder.mItem, PlaylistInfoProvider.PLAYLIST_CONTENT_URI, false);
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);

        Log.d(TAG, "onBindViewHolder() called with: " + "holder = [" + holder + "], position = [" + position + "], payloads = [" + payloads + "]");
        for(Object obj : payloads) {
            Log.d(TAG, "onBindViewHolder: " + obj);
            if (obj.equals(LoadingEvents.PROGRESS_CHANGED)) {
                Log.d(TAG, "onBindViewHolder: LoadingEvents.PROGRESS_CHANGED");
                if (holder.mItem.getStateHolder().getProgressValue() > 0) {
                    holder.progressBar.setIndeterminate(false);
                    holder.progressBar.setProgress(holder.mItem.getStateHolder().getProgressValue());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Observable observable, Object data) {

//        Log.d(TAG, "update: mValues.containsAll(PlaylistHolder.getInstance().getPlaylist()) = " + mValues.containsAll(PlaylistHolder.getInstance().getPlaylist()));
        if (observable instanceof PlaylistHolder) {
            Log.d(TAG, "update: PlaylistHolder");

            HashMap<PlayerEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>) ?
                    (HashMap<PlayerEvents, MusicData.Item>) data : null;

            if (eventsMap != null) {

                for (Map.Entry<PlayerEvents, MusicData.Item> entry : eventsMap.entrySet()) {
                    MusicData.Item item = entry.getValue();
                    int index = mValues.indexOf(item);
//                    Log.d(TAG, "update: item = " + item.toString());
//                    Log.d(TAG, "update: index = " + index);
                    if (index != -1)
                        notifyItemChanged(index);
                }
            }
        }
        if (observable instanceof DownloadedListHolder) {

            Log.d(TAG, "update: DownloadedListHolder");
            HashMap<LoadingEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>) ? (HashMap<LoadingEvents, MusicData.Item>) data : null;
            if (eventsMap != null) {

                Map.Entry<LoadingEvents, MusicData.Item> entry = eventsMap.entrySet().iterator().next();
                MusicData.Item item = entry.getValue();
                int index = mValues.indexOf(item);

                if (mValues.contains(item)) {
                    switch (entry.getKey()) {
                        case DOWNLOADED_NEW:
                            int toPosition = DownloadingListHolder.getInstance().getDownloadingList().size();
                            int replaceTo = toPosition + 1;

                            if (toPosition < 1)
                                break;

                            mValues.add(replaceTo, item);
                            mValues.remove(item);


                            notifyItemRemoved(index);
                            notifyItemInserted(toPosition);
                            break;

                        case REMOVE_DOWNLOADED:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DEFAULT);
                            mValues.remove(item);
                            notifyItemRemoved(index);
                            break;
                    }
                }
            }
        }

        if (observable instanceof DownloadingListHolder) {

            Log.d(TAG, "update: DownloadedListHolder");

            HashMap<LoadingEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>) ? (HashMap<LoadingEvents, MusicData.Item>) data : null;

            if (eventsMap != null) {

                Map.Entry<LoadingEvents, MusicData.Item> entry = eventsMap.entrySet().iterator().next();

                Log.d(TAG, "update: DownloadedListHolder  eventsMap != null");
                Log.d(TAG, "update: " + entry.getKey());
                MusicData.Item item = entry.getValue();
                int index = mValues.indexOf(item);

                if (mValues.contains(item)) {
                    switch (entry.getKey()) {
                        case PROGRESS_CHANGED:
                            item.getStateHolder().setProgressBarVisible(View.VISIBLE);
                            notifyItemChanged(index, LoadingEvents.PROGRESS_CHANGED);
                            break;

                        case LOADING_FAILED:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DEFAULT);
                            mValues.remove(item);
                            notifyItemRemoved(index);
                            break;

                        case LOADING_FINISHED:
                            Log.d(TAG, "update: LOADING_FINISHED");
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DOWNLOADED);
                            notifyItemChanged(index);
                            break;
                    }
                }
            }
        }
    }
}
