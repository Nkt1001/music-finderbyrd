package com.dgtprm.nkt10.musicfinderbyrd.model;

import com.dgtprm.nkt10.musicfinderbyrd.model.system.BAN_LIST;

import java.util.List;

public class GlobalBanList {
    private static GlobalBanList globalBanList;

    private List<Integer> banIds;

    private GlobalBanList() {
    }

    public static GlobalBanList initialize(BAN_LIST banList) {

        globalBanList = new GlobalBanList();
        globalBanList.setBanIds(banList.getResponse().getIds());
        return globalBanList;
    }

    public static GlobalBanList getInstance() {

        return globalBanList;
    }

    public List<Integer> getBanIds() {
        return banIds;
    }

    protected void setBanIds(List<Integer> banIds) {
        this.banIds = banIds;
    }
}
