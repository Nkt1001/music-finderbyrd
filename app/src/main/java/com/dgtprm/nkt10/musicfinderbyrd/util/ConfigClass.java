package com.dgtprm.nkt10.musicfinderbyrd.util;

import com.dgtprm.nkt10.musicfinderbyrd.BuildConfig;

public abstract class ConfigClass {
    public static boolean PERMISSION_DOWNLOADING = false;
    public static final String ACTION_RATE = "market://details?id=" + BuildConfig.APPLICATION_ID;
    public static final String URL_RATE = "http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;
}
