package com.dgtprm.nkt10.musicfinderbyrd.sql;

import android.provider.BaseColumns;

public class CreatePlaylistContract {
    private String TABLE_NAME;
    private String SQL_CREATE_ENTRIES;

    public CreatePlaylistContract(String tableName) {
        TABLE_NAME =  "[" + tableName + "]";
        SQL_CREATE_ENTRIES = "create table " + TABLE_NAME
                + "("
                + CreatePlaylistEntry._ID + " integer primary key autoincrement, "
                + CreatePlaylistEntry.DB_AUDIO_ID + " integer, "
                + CreatePlaylistEntry.DB_NAME + " text, "
                + CreatePlaylistEntry.DB_ARTIST + " text, "
                + CreatePlaylistEntry.DB_DURATION + " integer, "
                + CreatePlaylistEntry.DB_PATH_URI + " text, "
                + CreatePlaylistEntry.DB_PATH_URL + " text"
                + ");";
    }

    public String getSqlCreateScript() {
        return SQL_CREATE_ENTRIES;
    }

    public static abstract class CreatePlaylistEntry implements BaseColumns {

        public static final String DB_AUDIO_ID = "trackId";
        public static final String DB_NAME = "name";
        public static final String DB_ARTIST = "artist";
        public static final String DB_DURATION = "duration";
        public static final String DB_PATH_URI = "uri";
        public static final String DB_PATH_URL = "url";
    }
}
