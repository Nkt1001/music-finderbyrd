package com.dgtprm.nkt10.musicfinderbyrd.util.player;

import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.util.LoadingEvents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

public class DownloadingListHolder extends Observable {

    private static final String TAG = App.TAG + "." + DownloadingListHolder.class.getSimpleName();
    private static DownloadingListHolder singleton;
    private DownloadedListHolder downloadedListHolder;
    private HashMap<LoadingEvents, MusicData.Item> eventMap;

    private List<MusicData.Item> list;


    private DownloadingListHolder(){
        downloadedListHolder = DownloadedListHolder.getInstance();
        list = new ArrayList<>();
    }

    public static DownloadingListHolder getInstance(){
        if (singleton != null)
            return singleton;

        singleton = new DownloadingListHolder();
        return singleton;
    }

    public boolean addNewDownloading(MusicData.Item newItem){

//        Log.d(TAG, "addNewDownloading: " + newItem.toString());
        if (list.contains(newItem) || DownloadedListHolder.getInstance().containsItem(newItem))
            return false;

//        newItem.setProgressBarVisible(View.VISIBLE);
        list.add(newItem);



        eventMap = new HashMap<>(1);
        eventMap.put(LoadingEvents.LOADING_NEW, newItem);

        setChanged();
        notifyObservers(eventMap);
        return true;
    }

    public List<MusicData.Item> getDownloadingList(){
        return list;
    }

    public void changeProgress(MusicData.Item item, int progress){

        Log.d(TAG, "changeProgress: " + progress);
        item.getStateHolder().setProgressValue(progress);

        eventMap = new HashMap<>(1);
        eventMap.put(LoadingEvents.PROGRESS_CHANGED, item);

        setChanged();
        notifyObservers(eventMap);
    }

    public void downloadingFinished(MusicData.Item item) {

//        item.setProgressBarVisible(View.GONE);

        list.remove(item);

        eventMap = new HashMap<>(1);
        eventMap.put(LoadingEvents.LOADING_FINISHED, item);

        setChanged();
        notifyObservers(eventMap);
    }

    public void downloadingFailed(MusicData.Item item){

//        item.setProgressBarVisible(View.GONE);

        list.remove(item);

        eventMap = new HashMap<>(1);
        eventMap.put(LoadingEvents.LOADING_FAILED, item);

        setChanged();
        notifyObservers(eventMap);
    }


}
