package com.dgtprm.nkt10.musicfinderbyrd.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.PlayerActivity;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicItemStateHolder;
import com.dgtprm.nkt10.musicfinderbyrd.service.MediaService;
import com.dgtprm.nkt10.musicfinderbyrd.util.LoadingEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.SecondsToMinutesConverter;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlayerEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class PlayerFragment extends Fragment implements View.OnTouchListener, View.OnClickListener, Observer {

    private static final String TAG = App.TAG + "." + PlayerFragment.class.getSimpleName();
    private View rootView;

    private TextView tvArtist;
    private TextView tvTitle;
    private TextView tvTimer;

    private CircularProgressBar progressBar;

    private ImageButton imBNext;
    private ImageButton imBPrev;
    private FrameLayout imBInfo;
    private FrameLayout imBAddInPlaylist;
    private FrameLayout imBDownload;

    private ToggleButton togglePlaying;
    private ToggleButton toggleShuffle;
    private ToggleButton toggleRepeat;

    private SharedPreferences mPref;
    private volatile boolean isReleased;
    private long myTime;
    private long downTime;

    private Tracker mTracker;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {

                PlayerEvents event = (PlayerEvents) intent.getSerializableExtra(PlayerActivity.SERVICE_COMMAND);

                switch (event) {
                    case PLAYER_TICK:
                        int millis = intent.getIntExtra(MediaService.PROGRESS_MILLIS, 0);
                        float percents = intent.getFloatExtra(MediaService.PROGRESS_PERCENTS, 0);
                        setProgress(percents, millis);
                        break;
                    case PLAYER_PREPARING:
                        MusicData.Item item = intent.getParcelableExtra(MediaService.ITEM);
                        boolean isPaused = intent.getBooleanExtra(MediaService.EXTRA_IS_PAUSED, false);
                        Log.d(TAG, "onReceive: " + isPaused);
                        initTitle(item, isPaused);
                        break;
                    case PLAYER_PREPARED:
                        togglePlaying.setChecked(true);
                        break;
                }
            }
        }
    };



    private OnFragmentInteractionListener mListener;

    public PlayerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        App app = (App) getActivity().getApplication();
        mTracker = app.getDefaultTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_player, container, false);

        tvArtist = (TextView) rootView.findViewById(R.id.player_artist);
        tvTitle = (TextView) rootView.findViewById(R.id.player_title);
        tvTimer = (TextView) rootView.findViewById(R.id.player_timer);

        progressBar = (CircularProgressBar) rootView.findViewById(R.id.player_circularProgress);

        imBAddInPlaylist = (FrameLayout) rootView.findViewById(R.id.player_item_extra_add);

        imBDownload = (FrameLayout) rootView.findViewById(R.id.player_item_extra_download);
        imBInfo = (FrameLayout) rootView.findViewById(R.id.player_item_extra_info);
        imBPrev = (ImageButton) rootView.findViewById(R.id.player_prev);
        imBPrev.setOnTouchListener(this);
        imBNext = (ImageButton) rootView.findViewById(R.id.player_next);
        imBNext.setOnTouchListener(this);

        toggleRepeat = (ToggleButton) rootView.findViewById(R.id.player_repeat);
        toggleShuffle = (ToggleButton) rootView.findViewById(R.id.player_shuffle);
        togglePlaying = (ToggleButton) rootView.findViewById(R.id.player_playButton);
        togglePlaying.setChecked(true);

        setClickListener(imBAddInPlaylist, imBInfo, imBDownload);

        adjustToggle();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        DownloadingListHolder.getInstance().addObserver(this);
        DownloadedListHolder.getInstance().addObserver(this);
        getActivity().registerReceiver(receiver, new IntentFilter(PlayerActivity.ACTION_MUSIC));
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("PlayerActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void setClickListener(View... views){
        for (View v : views)
            v.setOnClickListener(this);
    }


    private void adjustToggle(){
        toggleShuffle.setChecked(mPref.getBoolean(getString(R.string.pref_shuffle), false));
        toggleRepeat.setChecked(mPref.getBoolean(getString(R.string.pref_repeat), false));

        toggleRepeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPref.edit().putBoolean(getString(R.string.pref_repeat), isChecked).apply();
                mListener.setRepeatMode(isChecked);
            }
        });

        toggleShuffle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPref.edit().putBoolean(getString(R.string.pref_shuffle), isChecked).apply();
                mListener.setShuffleMode(isChecked);
            }
        });

        togglePlaying.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    mListener.resumeMusic();
                else
                    mListener.pauseMusic();

            }
        });

    }

    private void initTitle(MusicData.Item item, boolean isPaused){

        if (item != null) {
            togglePlaying.setChecked(!isPaused);
            tvArtist.setText(item.getArtist());
            tvTitle.setText(item.getTitle());
            if (DownloadedListHolder.getInstance().getDownloadedList().contains(item)) {
                imBDownload.setBackgroundResource(R.drawable.delete_button_selector);
            } else{
                imBDownload.setBackgroundResource(R.drawable.playlist_item_extra_download_selector);
            }
        }
    }

    private void setProgress(float progressPercent, int progressValue){

        tvTimer.setText(SecondsToMinutesConverter.convert(Math.round(progressValue / 1000)));
        progressBar.setProgress(progressPercent);
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.d(TAG, "onStop: ");
        if (DownloadingListHolder.getInstance() != null)
            DownloadingListHolder.getInstance().deleteObserver(this);

        if (DownloadedListHolder.getInstance() != null)
            DownloadedListHolder.getInstance().deleteObserver(this);
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.player_item_extra_add:
                mListener.showPlaylistDialog(PlaylistHolder.getInstance().getLastPlaying());
                break;
            case R.id.player_item_extra_info:
                mListener.showInfo(PlaylistHolder.getInstance().getLastPlaying());
                break;
            case R.id.player_item_extra_download:
                if (PlaylistHolder.getInstance().getItemPlaying() != null)
                    mListener.downloadTrack(PlaylistHolder.getInstance().getItemPlaying());
                else if (PlaylistHolder.getInstance().getLastPlaying() != null)
                    mListener.downloadTrack(PlaylistHolder.getInstance().getLastPlaying());
                break;
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch(v.getId()){
            case R.id.player_next:

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    downTime = System.currentTimeMillis();
                    Log.d(TAG, "onTouch: MotionEvent.ACTION_DOWN");
                    v.setBackgroundResource(R.drawable.bt_play_control_3_);
                    isReleased = false;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            while(!isReleased){
                                long currentTime = System.currentTimeMillis();

                                if ((currentTime - myTime) > 500){
                                    myTime = currentTime;
                                    mListener.seekForward();
                                    Log.d(TAG, "onTouch: in cycle");
                                }
                            }
                        }
                    }).start();

                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.d(TAG, "onTouch: MotionEvent.ACTION_UP");
                    v.setBackgroundResource(R.drawable.bt_play_control_3);
                    isReleased = true;

                    if ((System.currentTimeMillis()-downTime) < 200 )
                        mListener.playNext();
                }
                return true;

            case R.id.player_prev:

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    downTime = System.currentTimeMillis();
                    v.setBackgroundResource(R.drawable.bt_play_control_2_);
                    isReleased = false;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            while(!isReleased){
                                long currentTime = System.currentTimeMillis();

                                if ((currentTime - myTime) > 500){
                                    myTime = currentTime;
                                    mListener.seekBackward();
                                    Log.d(TAG, "onTouch: in cycle");
                                }

                            }
                        }
                    }).start();

                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.d(TAG, "onTouch: MotionEvent.ACTION_UP");
                    v.setBackgroundResource(R.drawable.bt_play_control_2);
                    isReleased = true;

                    if ((System.currentTimeMillis()-downTime) < 200 )
                        mListener.playPrevious();

//                    startCountDownTimer();

                }
                return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof DownloadedListHolder){
            HashMap<LoadingEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>)
                    ? (HashMap<LoadingEvents, MusicData.Item>) data : null;
            if (eventsMap != null) {

                Map.Entry<LoadingEvents, MusicData.Item> entry = eventsMap.entrySet().iterator().next();
                MusicData.Item item = entry.getValue();

                if (item.equals(PlaylistHolder.getInstance().getItemPlaying())) {

                    switch (entry.getKey()) {
                        case REMOVE_DOWNLOADED:
                            Toast.makeText(getActivity(), "Removed " + item.toString(), Toast.LENGTH_SHORT).show();
                            imBDownload.setBackgroundResource(R.drawable.playlist_item_extra_download_selector);
                            break;
                    }
                }
            }
        }
        else if (observable instanceof DownloadingListHolder){
            HashMap<LoadingEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>)
                    ? (HashMap<LoadingEvents, MusicData.Item>) data : null;

            if (eventsMap != null) {

                Map.Entry<LoadingEvents, MusicData.Item> entry = eventsMap.entrySet().iterator().next();
                MusicData.Item item = entry.getValue();

                if (item.equals(PlaylistHolder.getInstance().getLastPlaying())) {

                    switch (entry.getKey()) {
                        case LOADING_NEW:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DOWNLOADING);
                            Log.d(TAG, "update: DownloadingListHolder LOADING_NEW");
                            Toast.makeText(getActivity(), "Loading " + item.getTitle(), Toast.LENGTH_SHORT).show();
                            break;
                        case LOADING_FAILED:
                            Toast.makeText(getActivity(), "Loading Failed", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "update: DownloadingListHolder LOADING_FAILED");

                            break;
                        case LOADING_FINISHED:
                            imBDownload.setBackgroundResource(R.drawable.delete_button_selector);
                            break;
                    }
                }
            }
        }

    }

    public interface OnFragmentInteractionListener {
        void playNext();
        void playPrevious();
        void seekForward();
        void seekBackward();
        void setShuffleMode(boolean what);
        void setRepeatMode(boolean what);
        void pauseMusic();
        void resumeMusic();
        void showInfo(MusicData.Item mItem);
        void downloadTrack(MusicData.Item mItem);
        void showPlaylistDialog(MusicData.Item item);
    }
}
