package com.dgtprm.nkt10.musicfinderbyrd;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.model.GlobalBanList;
import com.dgtprm.nkt10.musicfinderbyrd.model.system.API_ID;
import com.dgtprm.nkt10.musicfinderbyrd.model.system.BAN_LIST;
import com.dgtprm.nkt10.musicfinderbyrd.service.SystemIntentService;
import com.dgtprm.nkt10.musicfinderbyrd.util.PermissionsChecker;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = App.TAG + "." + SplashActivity.class.getSimpleName();
    private static final int RESULT_PERMISSIONS = 101;
    private SharedPreferences mPref;

    public static final String SYSTEM_BROADCAST_ACTION = "com.digitalpromo.filter.system";
    public static final String COMMAND_ID = "COMMAND_ID";
    public static final String COMMAND_BAN = "COMMAND_BAN";

    private IntentFilter intentFilter;
    private ResponseReceiver receiver;
    private boolean receivedId = false;
    private boolean receivedBanList = false;
    private boolean askPermissions;
    private boolean hasSavedMusic;

    private static final String[] PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.WAKE_LOCK,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.READ_CONTACTS};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mPref = PreferenceManager.getDefaultSharedPreferences(this);
        intentFilter = new IntentFilter(SYSTEM_BROADCAST_ACTION);
        receiver = new ResponseReceiver();
        askPermissions = true;
        hasSavedMusic = DownloadedListHolder.getInstance().getDownloadedList().size() != 0;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mPref.getBoolean(getString(R.string.pref_permissions), true))
            ((App)getApplication()).initDataPath(mPref);

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, intentFilter);

    }

    @Override
    protected void onStop() {
        super.onStop();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }

    private void checkMyPermissions() {

        PermissionsChecker checker = new PermissionsChecker(this);

        if (checker.lacksPermissions(PERMISSIONS) && askPermissions){
            startPermissionActivity();
        } else {
            int id = mPref.getInt(getString(R.string.pref_appId), -1);

            if (id != -1){
                ((App)getApplication()).initialize(id);
                receivedId = true;
            } else{
                SystemIntentService.startActionApiID(this, getString(R.string.api_list));
            }
            SystemIntentService.startActionBanList(this, getString(R.string.api_ban_list));
        }
    }

    private void startPermissionActivity(){
        PermissionsActivity.startActivityForResult(this, RESULT_PERMISSIONS, PERMISSIONS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult() called with: " + "requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");

        if (requestCode == RESULT_PERMISSIONS){
            switch (resultCode){
                case PermissionsActivity.PERMISSIONS_DENIED:
                    mPref.edit().putBoolean(getString(R.string.pref_permissions), false).apply();

                    break;
                case PermissionsActivity.PERMISSIONS_GRANTED:
                    ((App)getApplication()).initDataPath(mPref);
                    mPref.edit().putBoolean(getString(R.string.pref_permissions), true).apply();

                    break;
            }
            askPermissions = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkMyPermissions();

    }

    private void goToMain(){
        Log.d(TAG, "goToMain: ");

        if (!receivedId || !receivedBanList){
            return;
        }
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            this.finish();
    }

    private void showError(){
        Toast.makeText(SplashActivity.this, "Error. No Internet Connection. Try again", Toast.LENGTH_SHORT).show();

        Log.d(TAG, "showError: receivedId" + receivedId);
        Log.d(TAG, "showError: receivedBanList" + receivedBanList);
        Log.d(TAG, "showError: hasSavedMusic" + hasSavedMusic);
        if (receivedId && !receivedBanList && hasSavedMusic) {
            if (mPref.edit().putBoolean(getString(R.string.pref_allow_api), receivedBanList).commit()) {
                receivedBanList = true;
                goToMain();
            }
        }
    }



   private class ResponseReceiver extends BroadcastReceiver{

       private ResponseReceiver(){}

       @Override
       public void onReceive(Context context, Intent intent) {

           if (intent == null) return;

           if (intent.hasExtra(COMMAND_ID)) {
               API_ID id = intent.getParcelableExtra(COMMAND_ID);
               Log.d(TAG, "onReceive: id = " + id);
               if (id == null || !id.getStatus()){
                   receivedId = false;
                   showError();
                   return;
               }

               ((App)getApplication()).initialize(id.getResponse().getVkApiId());
               mPref.edit().putInt(getString(R.string.pref_appId), id.getResponse().getVkApiId()).apply();
               receivedId = true;
               goToMain();

           } else if (intent.hasExtra(COMMAND_BAN)) {
               BAN_LIST banList = intent.getParcelableExtra(COMMAND_BAN);

               Log.d(TAG, "onReceive: banlist " + banList);
               if (banList == null || !banList.getStatus()) {
                   receivedBanList = false;
                   showError();
                   return;
               }

               GlobalBanList.initialize(banList);
               receivedBanList = true;
               if (mPref.edit().putBoolean(getString(R.string.pref_allow_api), receivedBanList).commit())
                   goToMain();
           }
       }
   }
}
