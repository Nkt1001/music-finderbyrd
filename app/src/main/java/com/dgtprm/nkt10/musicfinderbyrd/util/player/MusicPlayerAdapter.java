package com.dgtprm.nkt10.musicfinderbyrd.util.player;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;

import java.io.IOException;
import java.util.Observable;

public class MusicPlayerAdapter extends Observable implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
    private static final String TAG = App.TAG + "." + MusicPlayerAdapter.class.getSimpleName();
    private MediaPlayer mediaPlayer;
    private AudioManager audioManager;
    private static MusicPlayerAdapter singleton;
    private boolean isLooping;

    private final int SEEK_TIME = 1000;

    private MusicPlayerAdapter(){
        Log.d(TAG, "MusicPlayerAdapter: ");
        mediaPlayer = new MediaPlayer();
    }

//    public MusicPlayerAdapter(Activity activity, MediaPlayer player){
//
//
//    }

    public static MusicPlayerAdapter getInstance(){
        if (singleton != null)
            return singleton;

        singleton = new MusicPlayerAdapter();
        return singleton;
    }

    public synchronized void playMusic(MusicData.Item item) throws IOException {

        Log.d(TAG, "playMusic: ");

        if(mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            Log.d(TAG, "playMusic: mediaplayer == null");
        }
        mediaPlayer.reset();

        PlaylistHolder.getInstance().setItemPlaying(item);
        mediaPlayer.setDataSource(item.getUrl());
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        Log.d(TAG, "playMusic: prepare async");
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.prepareAsync();
        mediaPlayer.setOnCompletionListener(this);

        setChanged();
        notifyObservers(PlayerEvents.PLAYER_PREPARING);
    }

    public synchronized void pauseMusic(){

        if(mediaPlayer == null) return;

        if(mediaPlayer.isPlaying())
            mediaPlayer.pause();

        setChanged();
        notifyObservers(PlayerEvents.PLAYER_PAUSE);
    }

    public synchronized void resumeMusic(){
        if(mediaPlayer == null) return;

        if(!mediaPlayer.isPlaying())
            mediaPlayer.start();

        setChanged();
        notifyObservers(PlayerEvents.PLAYER_PLAY);
    }

    public synchronized void stopMusic(){
        if(mediaPlayer == null) return;

        if(mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            PlaylistHolder.getInstance().setStopped();

            setChanged();
            notifyObservers(PlayerEvents.PLAYER_STOP);
        }
    }

    public void releaseMP(){
        Log.d(TAG, "releaseMP: ");
        if(null != mediaPlayer){
            try{
                mediaPlayer.release();
                mediaPlayer = null;
                PlaylistHolder.getInstance().setStopped();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public synchronized void seekForward(){
        if (mediaPlayer != null && mediaPlayer.isPlaying()){
            int duration = mediaPlayer.getDuration();
            int position = mediaPlayer.getCurrentPosition() + SEEK_TIME;
            if (position < duration){
                mediaPlayer.seekTo(position);
            }else {
                mediaPlayer.seekTo(duration);
            }
        }
    }

    public synchronized void seekBack(){
        if (mediaPlayer != null && mediaPlayer.isPlaying()){
            int position = mediaPlayer.getCurrentPosition() - SEEK_TIME;
            if (position > 0){
                mediaPlayer.seekTo(position);
            }else {
                mediaPlayer.seekTo(0);
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
//        logMusicInfo();
        Log.d(TAG, "onPrepared: ");
//        mp.setLooping(isLooping);
        mp.start();

        setChanged();
        notifyObservers(PlayerEvents.PLAYER_PREPARED);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
//        logMusicInfo();
        Log.d(TAG, "onCompletion: ");

        if (!isLooping)
            playNext();
        else {
            try {
                playMusic(PlaylistHolder.getInstance().getItemPlaying());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void setShuffle(boolean shuffle){
        PlaylistHolder.getInstance().setShuffleMode(shuffle);
    }

    public synchronized void setRepeat(boolean repeat){
        isLooping = repeat;
    }

    public synchronized boolean playNext(){
        Log.d(TAG, "playNext: ");
        PlaylistHolder holder = PlaylistHolder.getInstance();

        if (holder == null)
            return false;

        MusicData.Item next;

        if((holder.getCurrentPlaying()+1) < holder.getPlaylist().size()) {
            int i = holder.getCurrentPlaying();
            next = holder.getPlaylist().get(++i);
        } else {
            next = holder.getPlaylist().get(0);
        }

        try {
            playMusic(next);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public synchronized boolean playPrevious(){
        Log.d(TAG, "playPrevious: ");

        if (getCurrentPosition() > 3000){
            mediaPlayer.seekTo(0);
            return false;
        }


        PlaylistHolder holder = PlaylistHolder.getInstance();
        MusicData.Item prev=null;
        if(holder != null && (holder.getCurrentPlaying()-1 >= 0)) {
            int i = holder.getCurrentPlaying();
            prev = holder.getPlaylist().get(--i);
        } else if (holder != null) {
            prev = holder.getPlaylist().get(holder.getPlaylist().size()-1);
        }
        try {
            playMusic(prev);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public synchronized float getProgressValue(){
        Log.d(TAG, "getProgressValue: ");
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
//            Log.d(TAG, "getProgressValue: current = " + mediaPlayer.getCurrentPosition());
//            Log.d(TAG, "getProgressValue: duration = " + mediaPlayer.getDuration());

            return 100 * mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration();
        }
        return 0;
    }

    public synchronized int getCurrentPosition(){
        Log.d(TAG, "getCurrentPosition: ");
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            return mediaPlayer.getCurrentPosition();

        return 0;
    }

    public synchronized int getRemainingTime(){
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            return mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition();

        return (1000 * PlaylistHolder.getInstance().getItemPlaying().getDuration());
    }

    private void logMusicInfo(){
        Log.d(TAG, "Playing " + mediaPlayer.isPlaying());
        Log.d(TAG, "Time " + mediaPlayer.getCurrentPosition() + " / "
                + mediaPlayer.getDuration());
        Log.d(TAG, "Looping " + mediaPlayer.isLooping());
//        Log.d(TAG,
//                "Volume " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
    }
}
