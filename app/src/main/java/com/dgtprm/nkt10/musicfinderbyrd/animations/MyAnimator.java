package com.dgtprm.nkt10.musicfinderbyrd.animations;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;

import com.dgtprm.nkt10.musicfinderbyrd.R;

public class MyAnimator {

    public void openExtraAnimation(final View icMore){
        icMore.animate().rotation(-90).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                animation.setInterpolator(new AccelerateDecelerateInterpolator());
                animation.setDuration(150);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                icMore.setRotation(0);
                ((ImageView) icMore).setImageResource(R.drawable.ic_more_hor_24px);
            }
        }).start();
    }

    public void closeExtraAnimation(final View icMore){
        if (icMore.getId() != R.id.playlist_item_extra_icon)
            return;

        icMore.animate().rotation(90).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);

                animation.setInterpolator(new AccelerateDecelerateInterpolator());
                animation.setDuration(150);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                icMore.setRotation(0);
                ((ImageView)icMore).setImageResource(R.drawable.ic_more_vert);
            }
        }).start();
    }
}
