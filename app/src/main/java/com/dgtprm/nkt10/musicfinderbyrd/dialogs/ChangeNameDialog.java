package com.dgtprm.nkt10.musicfinderbyrd.dialogs;


import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.model.PlaylistItem;
import com.google.android.gms.analytics.HitBuilders;

public class ChangeNameDialog extends CreatePlaylistDialog{

    public static ChangeNameDialog newInstance(String title, String text) {

        ChangeNameDialog fragment = new ChangeNameDialog();
        fragment.setArguments(getBundle(title, text));
        return fragment;
    }

    @Override
    protected void doSomething() {
        PlaylistItem item = new PlaylistItem(edT.getText().toString());

        if (!mListener.isExist(item)){
            if (edT.getText().toString().length() == 0) return;

            Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
            mListener.nameChanged(mText, edT.getText().toString());
        } else{
            Toast.makeText(getActivity(), "Playlist already exists", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("ChangeNameDialog");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
