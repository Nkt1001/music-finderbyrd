package com.dgtprm.nkt10.musicfinderbyrd.adapters;

import android.util.Log;
import android.view.View;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.BasePlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicItemStateHolder;
import com.dgtprm.nkt10.musicfinderbyrd.sql.PlaylistInfoProvider;
import com.dgtprm.nkt10.musicfinderbyrd.util.LoadingEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlayerEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

public class VkPlaylistAdapter extends BaseRecyclerViewAdapter {

    private static final String TAG = App.TAG + "." + VkPlaylistAdapter.class.getSimpleName();

    public VkPlaylistAdapter(List<MusicData.Item> items, BasePlaylistFragment.PlaylistFragmentInteractionListener listener) {
        super(items, listener);
    }

    @Override
    protected void subscribeOnObservables()  {
        if (PlaylistHolder.getInstance() == null) {
            PlaylistHolder.newInstance(new ArrayList<MusicData.Item>());
        }
        PlaylistHolder.getInstance().addObserver(this);
        DownloadingListHolder.getInstance().addObserver(this);
        DownloadedListHolder.getInstance().addObserver(this);
    }

    @Override
    protected void bindPlayButton(ViewHolder holder) {
        holder.playStopImage.setImageResource(holder.mItem.equals(PlaylistHolder.getInstance().getItemPlaying())
                ? R.drawable.ic_pause_ : R.drawable.bt_track_play);
        holder.orangeLine.setVisibility(holder.mItem.equals(PlaylistHolder.getInstance().getItemPlaying())
                ? View.VISIBLE : View.INVISIBLE);

        holder.loadingButton.setBackgroundResource(holder.mItem.getStateHolder().getLoadingButtonRes());
    }

    @Override
    protected void setListeners(final ViewHolder holder) {
        holder.extraButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onExtraButtonPressed(holder);

                }
            }
        });

        holder.playButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mListener) {
                    mListener.onPlayButtonPressed(holder.mItem, mValues, false);

                }
            }
        });

        holder.loadingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mListener){
                    mListener.onDownloadRemoveButtonPressed(holder.mItem, PlaylistInfoProvider.PLAYLIST_CONTENT_URI, false);
                }
            }
        });
    }

    public void addNewPage(List<MusicData.Item> list){

        mValues.addAll(list);

        notifyItemRangeInserted(mValues.size()-list.size(), list.size());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Observable observable, Object data) {

        if (observable instanceof PlaylistHolder) {
            Log.d(TAG, "update: PlaylistHolder");

            HashMap<PlayerEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>) ?
                    (HashMap<PlayerEvents, MusicData.Item>) data : null;

            if (eventsMap != null) {

                for (Map.Entry<PlayerEvents, MusicData.Item> entry : eventsMap.entrySet()) {
                    MusicData.Item item = entry.getValue();
                    int index = mValues.indexOf(item);
                    if (index != -1)
                        notifyItemChanged(index);
                }
            }
        }
        if (observable instanceof DownloadedListHolder) {

            Log.d(TAG, "update: DownloadedListHolder");
            HashMap<LoadingEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>) ? (HashMap<LoadingEvents, MusicData.Item>) data : null;
            if (eventsMap != null) {

                Map.Entry<LoadingEvents, MusicData.Item> entry = eventsMap.entrySet().iterator().next();
                MusicData.Item item = entry.getValue();
                int index = mValues.indexOf(item);

                if (mValues.contains(item)) {
                    switch (entry.getKey()) {
                        case REMOVE_DOWNLOADED:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DEFAULT);
                            notifyItemChanged(index);
                            break;
                    }
                }
            }
        }

        if (observable instanceof DownloadingListHolder) {

            Log.d(TAG, "update: DownloadingListHolder");

            HashMap<LoadingEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>) ? (HashMap<LoadingEvents, MusicData.Item>) data : null;

            if (eventsMap != null) {

                Map.Entry<LoadingEvents, MusicData.Item> entry = eventsMap.entrySet().iterator().next();
                MusicData.Item item = entry.getValue();
                int index = mValues.indexOf(item);

                if (mValues.contains(item)){
                    switch (entry.getKey()) {
                        case LOADING_NEW:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DOWNLOADING);
//                            item.getStateHolder().hideExtraMenu();
                            notifyItemChanged(index);
                            break;
                        case LOADING_FAILED:
//                            item.getStateHolder().setStickerVisibility(false);
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DEFAULT);
                            notifyItemChanged(index);
                            break;
                        case LOADING_FINISHED:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DOWNLOADED);
                            notifyItemChanged(index);
                            break;
                    }
                }
            }
        }
    }
}
