package com.dgtprm.nkt10.musicfinderbyrd.fragments;

import android.os.Bundle;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.adapters.DownloadedPlaylistAdapter;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;
import com.google.android.gms.analytics.HitBuilders;

import java.util.ArrayList;
import java.util.List;

public class DownloadedMusicFragment extends BasePlaylistFragment {

    private static final String TAG = App.TAG + "." + DownloadedListHolder.class.getSimpleName();

    public static final String EXTRA_DOWNLOADED_DATA = "EXTRA_DOWNLOADED_DATA";

    public static DownloadedMusicFragment newInstance(ArrayList<MusicData.Item> items) {

        Bundle args = new Bundle();

        args.putParcelableArrayList(EXTRA_DOWNLOADED_DATA, items);
        DownloadedMusicFragment fragment = new DownloadedMusicFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public DownloadedMusicFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<MusicData.Item> argData = getArguments().getParcelableArrayList(EXTRA_DOWNLOADED_DATA);

        if (argData == null) {
            myData = new ArrayList<>(DownloadedListHolder.getInstance().getDownloadedList());
            myData.addAll(0, DownloadingListHolder.getInstance().getDownloadingList());
        } else{
            myData = argData;
        }

        closeExtraMenu(myData);
    }

    private void closeExtraMenu(List<MusicData.Item> data){
        for (MusicData.Item item : data)
            item.getStateHolder().hideExtraMenu();
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("Downloaded Music Fragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void setmAdapter() {
        recyclerView.setAdapter(new DownloadedPlaylistAdapter(myData, mListener));
    }
}
