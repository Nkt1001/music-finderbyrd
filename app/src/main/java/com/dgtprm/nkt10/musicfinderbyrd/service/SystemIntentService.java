package com.dgtprm.nkt10.musicfinderbyrd.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.SplashActivity;
import com.dgtprm.nkt10.musicfinderbyrd.model.system.API_ID;
import com.dgtprm.nkt10.musicfinderbyrd.model.system.BAN_LIST;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SystemIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_API_ID = "com.dgtprm.nkt10.musicfinderbyrd.service.action.APIID";
    private static final String ACTION_BAN_LIST = "com.dgtprm.nkt10.musicfinderbyrd.service.action.BANLIST";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM_URL = "com.dgtprm.nkt10.musicfinderbyrd.service.extra.URL";
    private static final String TAG = App.TAG + "." + SystemIntentService.class.getSimpleName();

    public SystemIntentService() {
        super("SystemIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionApiID(Context context, String url) {
        Log.d(TAG, "startActionApiID: ");
        Intent intent = new Intent(context, SystemIntentService.class);
        intent.setAction(ACTION_API_ID);
        intent.putExtra(EXTRA_PARAM_URL, url);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBanList(Context context, String url) {
        Log.d(TAG, "startActionBanList: ");
        Intent intent = new Intent(context, SystemIntentService.class);
        intent.setAction(ACTION_BAN_LIST);
        intent.putExtra(EXTRA_PARAM_URL, url);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_API_ID.equals(action)) {
                final String param = intent.getStringExtra(EXTRA_PARAM_URL);
                handleActionApiID(param);
            } else if (ACTION_BAN_LIST.equals(action)) {
                final String param = intent.getStringExtra(EXTRA_PARAM_URL);
                handleActionBanList(param);
            }
        }
    }

    private void handleActionApiID(String param) {
        Log.d(TAG, "handleActionApiID() called with: " + "param = [" + param + "]");

        API_ID myId=null;
        try {
            URL url = new URL(param);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                InputStream in = connection.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                StringBuilder sb = new StringBuilder();

                String s;
                while ((s=reader.readLine()) != null){
                    sb.append(s);
                }

                String response = sb.toString();
                myId = new Gson().fromJson(response, API_ID.class);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            Intent localIntent = new Intent(SplashActivity.SYSTEM_BROADCAST_ACTION);
            localIntent.putExtra(SplashActivity.COMMAND_ID, myId);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }
    }


    private void handleActionBanList(String param) {
        Log.d(TAG, "handleActionBanList() called with: " + "param = [" + param + "]");

        BAN_LIST banList=null;
        try {
            URL url = new URL(param);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                InputStream in = connection.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                StringBuilder sb = new StringBuilder();

                String s;
                while ((s=reader.readLine()) != null){
                    sb.append(s);
                }

                String response = sb.toString();
                banList = new Gson().fromJson(response, BAN_LIST.class);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            Intent localIntent = new Intent(SplashActivity.SYSTEM_BROADCAST_ACTION);
            localIntent.putExtra(SplashActivity.COMMAND_BAN, banList);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }
    }
}
