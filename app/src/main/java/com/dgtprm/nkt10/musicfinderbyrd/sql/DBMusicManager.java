package com.dgtprm.nkt10.musicfinderbyrd.sql;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.sql.SavedAudioReaderContract.PlaylistReaderEntry;

import java.io.File;
import java.util.List;

public class DBMusicManager {
    private static final String TAG = App.TAG + "." + DBMusicManager.class.getSimpleName();
    private Context mContext;
    private static final String SQL_DELETE_TABLE_FORMAT = "DROP TABLE IF EXISTS '%s'";
    private static final String SQL_RENAME_TABLE_FORMAT = "ALTER TABLE '%s' RENAME TO '%s'";

    public DBMusicManager(Context context) {
        mContext = context;
    }

    private ContentResolver getContentResolver(){
        return mContext.getContentResolver();
    }

    public void insert(Uri uri, MusicData.Item item){
        Log.d(TAG, "insert: " + item.getTitle());
        ContentValues cv = new ContentValues();

        cv.put(PlaylistReaderEntry.DB_ARTIST, item.getArtist());
        cv.put(PlaylistReaderEntry.DB_AUDIO_ID, item.getId());
        cv.put(PlaylistReaderEntry.DB_DURATION, item.getDuration());
        cv.put(PlaylistReaderEntry.DB_NAME, item.getTitle());
        cv.put(PlaylistReaderEntry.DB_PATH_URL, item.getUrl());
        cv.put(PlaylistReaderEntry.DB_PATH_URI, item.getUri());

        getContentResolver().insert(uri, cv);
    }

    public void deleteItem(MusicData.Item item){
        Log.d(TAG, "deleteItem: " + item.getUri());
        Uri uri = ContentUris.withAppendedId(PlaylistInfoProvider.PLAYLIST_CONTENT_URI, item.getId());

        Log.d(TAG, "deleteItem: " + uri.toString());
        getContentResolver().delete(uri, null, null);

        if (new File(item.getUri()).delete()) {
            Log.d(TAG, "deleteItem: success");
            item.setUri("");
        }
    }

    public void deleteAll(Uri uri){
        Log.d(TAG, "deleteAll: " + uri.toString());

        getContentResolver().delete(uri, null, null);
    }

    public void deleteList(Uri uri, List<MusicData.Item> deleteList){
        String andArg = " AND ";
        String whereClause = PlaylistReaderEntry.DB_AUDIO_ID + " = ?";
        String[] whereArgs = new String[deleteList.size()];
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < deleteList.size(); i++){
            if (deleteList.size() >= 1) {
                sb.append(whereClause);

                if (i+1 < deleteList.size())
                    sb.append(andArg);
            }
            whereArgs[i] = String.valueOf(deleteList.get(i).getId());
        }

        whereClause = sb.toString();

        getContentResolver().delete(uri, whereClause, whereArgs);
    }

    public void update(MusicData.Item item, String column, String value){
        Uri uri = ContentUris.withAppendedId(PlaylistInfoProvider.PLAYLIST_CONTENT_URI, item.getId());
        ContentValues cv = new ContentValues();

        cv.put(column, value);

        getContentResolver().update(uri, cv, null, null);
    }

    public void createTable(String name){
        CreatePlaylistContract contract = new CreatePlaylistContract(name);
        executeSQL(contract.getSqlCreateScript());
    }

    public void changeTableName(String old, String _new){
        String sqlQuery = String.format(SQL_RENAME_TABLE_FORMAT, old, _new);
        executeSQL(sqlQuery);
    }

    public void removeTable(String which){
        String sqlQuery = String.format(SQL_DELETE_TABLE_FORMAT, which);
        executeSQL(sqlQuery);
    }

    public void deleteItemFromTable(Uri uri, MusicData.Item item){

        String where = "_id = ?";
        String[] whereArgs = new String[]{String.valueOf(item.get_id())};
        getContentResolver().delete(uri, where, whereArgs);
    }

    private void executeSQL(String sqlQuery) {
        SQLHelper helper = new SQLHelper(mContext);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        try{

            db.execSQL(sqlQuery);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            helper.close();
        }
    }
}
