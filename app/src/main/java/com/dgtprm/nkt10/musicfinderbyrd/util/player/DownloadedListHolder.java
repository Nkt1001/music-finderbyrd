package com.dgtprm.nkt10.musicfinderbyrd.util.player;


import android.content.Context;
import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicItemStateHolder;
import com.dgtprm.nkt10.musicfinderbyrd.sql.DBMusicManager;
import com.dgtprm.nkt10.musicfinderbyrd.sql.PlaylistInfoProvider;
import com.dgtprm.nkt10.musicfinderbyrd.util.LoadingEvents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

public class DownloadedListHolder extends Observable {

    private static final String TAG = App.TAG + "." + DownloadedListHolder.class.getSimpleName();
    private static DownloadedListHolder singleton;
    private HashMap<LoadingEvents, MusicData.Item> eventMap;

    private List<MusicData.Item> list;

    private static DBMusicManager dbManager;

    private DownloadedListHolder(){
    }

    private DownloadedListHolder(List<MusicData.Item> items){
        list = items;
    }

    public static void initDownloadedList(Context context, List<MusicData.Item> myList){
        //init in application
        if (singleton != null) {
            Log.e(TAG, "initDownloadedList: OBJECT HAS BEEN INITIALIZED ALREADY", new Throwable("call getInstance instead of init"));
            return;
        }

        if (myList != null) {
            singleton = new DownloadedListHolder(myList);
        } else{
            singleton = new DownloadedListHolder(new ArrayList<MusicData.Item>());
        }
        dbManager = new DBMusicManager(context);
    }

    public static DownloadedListHolder getInstance(){
        if (singleton == null)
            Log.e(TAG, "getInstance: OBJECT HAS NOT BEEN INITIALIZED YES", new Throwable("call initDownloadList instead of getInstance"));

        return singleton;
    }

    public boolean containsItem(MusicData.Item item){
        return list.contains(item);
    }

    public void addNewDownloaded(MusicData.Item newItem){
        if(containsItem(newItem)){
            return;
        }

        if (PlaylistHolder.getInstance() != null && PlaylistHolder.getInstance().getPlaylist().contains(newItem)){
            int index = PlaylistHolder.getInstance().getPlaylist().indexOf(newItem);
            PlaylistHolder.getInstance().getPlaylist().get(index)
                    .setUri(newItem.getUri());
            PlaylistHolder.getInstance().getPlaylist().get(index)
                    .setStateHolder(MusicItemStateHolder.State.STATE_DOWNLOADED);
        }

        Log.d(TAG, "addNewDownloaded() called with: " + "newItem = [" + newItem + "]");
        list.add(0, newItem);

        eventMap = new HashMap<>(1);
        eventMap.put(LoadingEvents.DOWNLOADED_NEW, newItem);

        dbManager.insert(PlaylistInfoProvider.PLAYLIST_CONTENT_URI, newItem);

        setChanged();
        notifyObservers(eventMap);
    }

    public List<MusicData.Item> getDownloadedList(){
        return list;
    }

    public void removeItem(MusicData.Item item){
        if (!list.contains(item)) return;

            list.remove(item);

            eventMap = new HashMap<>(1);
            eventMap.put(LoadingEvents.REMOVE_DOWNLOADED, item);

            dbManager.deleteItem(item);

            setChanged();
            notifyObservers(eventMap);

    }
}
