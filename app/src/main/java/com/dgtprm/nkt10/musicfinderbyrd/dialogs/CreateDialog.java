package com.dgtprm.nkt10.musicfinderbyrd.dialogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.google.android.gms.analytics.Tracker;

public abstract class CreateDialog extends DialogFragment {

    protected static final String EXTRA_TEXT_DIALOG = "EXTRA_TEXT_DIALOG";
    protected static final String EXTRA_TITLE_DIALOG = "EXTRA_TITLE_DIALOG";

    protected EditText edT;

    protected String mText;
    protected String mTitle;

    CreateButtonClickListener mListener;

    protected Tracker mTracker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mText = getArguments().getString(EXTRA_TEXT_DIALOG, "");
        mTitle = getArguments().getString(EXTRA_TITLE_DIALOG, "");

        App app = (App) getActivity().getApplication();
        mTracker = app.getDefaultTracker();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setTitle(mTitle);
        View v = inflater.inflate(R.layout.layout_path_dialog, container, false);

        edT = (EditText) v.findViewById(R.id.path_dialog_edT);
        edT.setHint(mText);

        Button button = (Button) v.findViewById(R.id.path_dialog_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSomething();
                getDialog().dismiss();
            }
        });

        return v;
    }

    protected static Bundle getBundle(String title, String text){
        Bundle b = new Bundle();
        b.putString(EXTRA_TITLE_DIALOG, title);
        b.putString(EXTRA_TEXT_DIALOG, text);
        return b;
    }

    protected abstract void doSomething();

    public void addCreateClickListener(CreateButtonClickListener listener){
        mListener = listener;

    }

    public interface CreateButtonClickListener{
        void onCreateClicked(String newText);
    }
}
