package com.dgtprm.nkt10.musicfinderbyrd.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.SubscriptionsFragment;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.dummy.DummyContent.DummyItem;
import com.dgtprm.nkt10.musicfinderbyrd.model.SubscriptionsObject;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link SubscriptionsFragment.OnItemClickListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MysubscribeRecyclerViewAdapter extends RecyclerView.Adapter<MysubscribeRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = App.TAG + "." + MysubscribeRecyclerViewAdapter.class.getSimpleName();
    private final Context mContext;
    private final List<SubscriptionsObject.Item> mValues;
    private final SubscriptionsFragment.OnItemClickListener mListener;

    public MysubscribeRecyclerViewAdapter(List<SubscriptionsObject.Item> items, SubscriptionsFragment.OnItemClickListener listener, Context cotext) {
        mValues = items;
        mListener = listener;
        mContext = cotext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_subscribe, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        Log.d(TAG, "onBindViewHolder: " + mValues.get(position).getId());

        if(mValues.get(position).getName() == null)
            holder.name.setText(mValues.get(position).getFirstName() + " " + mValues.get(position).getLastName());
        else
            holder.name.setText(mValues.get(position).getName());

        Picasso.with(mContext).load(mValues.get(position).getPhotoUrl()).placeholder(R.drawable.ic_user_avatar_min).into(holder.imVAvatar);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onItemClick(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView name;
        public final TextView number;
        public final ImageView imVAvatar;

        public SubscriptionsObject.Item mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            name = (TextView) view.findViewById(R.id.subscribe_name);
            number = (TextView) view.findViewById(R.id.subscribe_number);
            imVAvatar = (ImageView) view.findViewById(R.id.subscribe_avatar);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "' - " + number.getText();
        }
    }
}
