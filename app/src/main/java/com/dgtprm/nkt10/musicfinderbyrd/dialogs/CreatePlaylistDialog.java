package com.dgtprm.nkt10.musicfinderbyrd.dialogs;

import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.model.PlaylistItem;
import com.google.android.gms.analytics.HitBuilders;

public class CreatePlaylistDialog extends CreateDialog {

    private static final String TAG = App.TAG + "." + CreatePlaylistDialog.class.getSimpleName();

    protected ChangeNameListener mListener;

    public static CreatePlaylistDialog newInstance(String title, String text) {

        CreatePlaylistDialog fragment = new CreatePlaylistDialog();

        fragment.setArguments(getBundle(title, text));
        return fragment;
    }

    @Override
    protected void doSomething() {

        PlaylistItem item = new PlaylistItem(edT.getText().toString());

        if (!mListener.isExist(item)){
            if (edT.getText().toString().length() == 0) return;

            Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
            mListener.onCreateClicked(edT.getText().toString());
        }
        else{
            Toast.makeText(getActivity(), "Playlist already exists", Toast.LENGTH_SHORT).show();
        }
    }

    public void addChangeNameListener(ChangeNameListener listener){
        mListener = listener;
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("CreatePlaylistDialog");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public interface ChangeNameListener extends CreateDialog.CreateButtonClickListener{
        void nameChanged(String oldName, String newName);
        boolean isExist(PlaylistItem item);
    }
}
