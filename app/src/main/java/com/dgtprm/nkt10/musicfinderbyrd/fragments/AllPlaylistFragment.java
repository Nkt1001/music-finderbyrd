package com.dgtprm.nkt10.musicfinderbyrd.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.dialogs.ChangeNameDialog;
import com.dgtprm.nkt10.musicfinderbyrd.dialogs.CreatePlaylistDialog;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicItemStateHolder;
import com.dgtprm.nkt10.musicfinderbyrd.model.PlaylistItem;
import com.dgtprm.nkt10.musicfinderbyrd.sql.DBMusicManager;
import com.dgtprm.nkt10.musicfinderbyrd.sql.PlaylistInfoProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllPlaylistFragment extends Fragment implements View.OnClickListener, CreatePlaylistDialog.ChangeNameListener,
        AdapterView.OnItemClickListener{

    private static final String TAG = App.TAG + "." + AllPlaylistFragment.class.getSimpleName();

    protected SimpleAdapter mAdapter;

    private PlaylistSelectedListener mListener;
    private SharedPreferences mPrefs;

    private ArrayList<Map<String, Object>> data;
    protected List<PlaylistItem> playlistArray;

    protected final String ATTRIBUTE_TITLE = "title";
    protected final String ATTRIBUTE_COUNT = "count";

    private final int CM_CHANGE_NAME = 0;
    private final int CM_DELETE = 1;

    protected boolean hasChanged;

    protected DBMusicManager dbManager;



    public AllPlaylistFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbManager = new DBMusicManager(getActivity());
        App app = (App) getActivity().getApplication();

        hasChanged = false;

        mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String playlistData = mPrefs.getString(getString(R.string.pref_my_playlist), "");

        data = new ArrayList<>();
        playlistArray = new ArrayList<>();

        initPlaylist(app, playlistData);
//        checkTables();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ListView listView = (ListView) inflater.inflate(R.layout.fragment_allplaylist_list, container, false);

        View headerView = inflater.inflate(R.layout.create_playlist_header, listView, false);

        headerView.setLayoutParams(new AbsListView.LayoutParams(headerView.getLayoutParams()));

        headerView.findViewById(R.id.create_playlist_imB).setOnClickListener(this);

        listView.addHeaderView(headerView);

        String[] from = new String[]{ATTRIBUTE_TITLE, ATTRIBUTE_COUNT};
        int[] to = new int[]{R.id.allPlaylist_name, R.id.allPlaylist_number};


        mAdapter = new SimpleAdapter(getActivity(), data, R.layout.fragment_allplaylist, from, to);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(this);

        registerForContextMenu(listView);
        return listView;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof PlaylistSelectedListener) {
            mListener = (PlaylistSelectedListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        if (hasChanged)
            serializeData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void serializeData() {
        Type mType = new TypeToken<ArrayList<PlaylistItem>>(){}.getType();
        String serializing = new Gson().toJson(playlistArray, mType);
        Log.d(TAG, "serializeData: " + serializing);
        mPrefs.edit().putString(getString(R.string.pref_my_playlist), serializing).apply();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.d(TAG, "onCreateContextMenu: ");
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(Menu.NONE, CM_CHANGE_NAME, Menu.NONE, R.string.allPlaylists_change_name);
        menu.add(Menu.NONE, CM_DELETE, Menu.NONE, R.string.allPlaylists_delete_item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = menuInfo.position - 1;

        if (item.getItemId() == CM_DELETE){
            removePlaylist(position);
            mAdapter.notifyDataSetChanged();
        } else if (item.getItemId() == CM_CHANGE_NAME){
            showChangeDialog(getString(R.string.allPlaylists_change_name), playlistArray.get(position).getName());
        }

        return super.onContextItemSelected(item);
    }

    protected void showDialog(String title, String text){
        Log.d(TAG, "showDialog: ");

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if(prev != null){
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        CreatePlaylistDialog dialog = CreatePlaylistDialog.newInstance(title, text);
        dialog.addChangeNameListener(this);
        dialog.show(ft, "dialog");
    }

    private void showChangeDialog(String title, String text){
        Log.d(TAG, "showDialog: ");

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if(prev != null){
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        ChangeNameDialog dialog = ChangeNameDialog.newInstance(title, text);
        dialog.addChangeNameListener(this);
        dialog.show(ft, "dialog");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.create_playlist_imB){
            showDialog(getString(R.string.allPlaylists_title), getString(R.string.allPlaylists_dialog_hint));
        }
    }

    @Override
    public void onCreateClicked(String newText) {

        hasChanged = true;
        PlaylistItem createdPlaylist = new PlaylistItem(newText);

        saveInSystem(createdPlaylist);

        mAdapter.notifyDataSetChanged();
    }

    protected void saveInSystem(PlaylistItem createdPlaylist) {

        dbManager.createTable(createdPlaylist.getTableName());

        playlistArray.add(createdPlaylist);

        Map<String, Object> m = new HashMap<>();
        m.put(ATTRIBUTE_TITLE, createdPlaylist.getName());
        m.put(ATTRIBUTE_COUNT, createdPlaylist.getCount());

        data.add(m);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        mListener.playlistSelected(playlistArray.get(position-1));
    }

    private void initPlaylist(App app, String playlistData) {
        if (!playlistData.isEmpty()){
            Type mType = new TypeToken<List<PlaylistItem>>(){}.getType();
            playlistArray = new Gson().fromJson(playlistData, mType);

            Map<String, Object> m;
            Cursor c;
            for (PlaylistItem item : playlistArray) {
                m = new HashMap<>();

                int cnt;
                String title;

                Uri uri = Uri.parse(PlaylistInfoProvider.MY_PLAYLIST_CONTENT_URI.toString() + "/" + item.getTableName());
                c = getActivity().getContentResolver().query(uri, null, null, null, null);

                List<MusicData.Item> musicList = app.getItemList(c, MusicItemStateHolder.State.STATE_PLAYLIST_ITEM);

                Log.d(TAG, "initPlaylist: " + musicList);

//                List<MusicData.Item> deleteList;
//
//                if (c != null) {
//                    musicList = new ArrayList<>();
//                    deleteList = new ArrayList<>();
//
//                    if (c.moveToFirst()){
//                        do {
//                            MusicData.Item musicItem = app.getItem(c);
//
//                            if (new File(musicItem.getUri()).exists()){
//                                musicList.add(musicItem);
//                            } else {
//                                deleteList.add(musicItem);
//                            }
//                        } while (c.moveToNext());
//                    }
//
//                    c.close();
//
//                    if (deleteList.size() > 0){
//                        dbManager.deleteList(uri, deleteList);
//                    }
//            }

                item.setMusicList(musicList);

                title = item.getName();
                cnt = item.getMusicList().size();

                m.put(ATTRIBUTE_TITLE, title);
                m.put(ATTRIBUTE_COUNT, cnt);
                data.add(m);
            }
        }
    }

    @Override
    public void nameChanged(String oldName, String newName) {
        hasChanged = true;

        dbManager.changeTableName(PlaylistItem.generateTableName(oldName),
                PlaylistItem.generateTableName(newName));

        int index = playlistArray.indexOf(new PlaylistItem(oldName));

        PlaylistItem renamedItem = playlistArray.get(index);
        renamedItem.setName(newName);

        data.remove(index);
        Map<String, Object> m = new HashMap<>();
        m.put(ATTRIBUTE_TITLE, renamedItem.getName());
        m.put(ATTRIBUTE_COUNT, renamedItem.getCount());
        data.add(index, m);
        mAdapter.notifyDataSetChanged();
    }

    private void removePlaylist(int position){
        hasChanged = true;

        PlaylistItem removingItem = playlistArray.get(position);

        playlistArray.remove(removingItem);

        data.remove(position);

        dbManager.removeTable(removingItem.getTableName());
    }

    @Override
    public boolean isExist(PlaylistItem item) {
        return playlistArray.contains(item);
    }


    public interface PlaylistSelectedListener {
        void playlistSelected(PlaylistItem item);
    }
}