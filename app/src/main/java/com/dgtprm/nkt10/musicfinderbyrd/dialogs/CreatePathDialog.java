package com.dgtprm.nkt10.musicfinderbyrd.dialogs;

import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.google.android.gms.analytics.HitBuilders;

public class CreatePathDialog extends CreateDialog {

    public static CreatePathDialog newInstance(String title, String text) {
        CreatePathDialog fragment = new CreatePathDialog();

        fragment.setArguments(getBundle(title, text));
        return fragment;
    }

    @Override
    protected void doSomething() {
        App app = (App) getActivity().getApplication();
        if(app.createDirIfNotExtists(edT.getText().toString())) {
            Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
            mListener.onCreateClicked(edT.getText().toString());
            getDialog().dismiss();
        } else{
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            getDialog().dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("CreatePathDialog");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
