package com.dgtprm.nkt10.musicfinderbyrd.dialogs;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.AllPlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.PlaylistItem;
import com.dgtprm.nkt10.musicfinderbyrd.sql.PlaylistInfoProvider;

import java.util.List;

/**
 * Created by Администратор on 08.04.2016.
 */
public class AddToPlaylistDialog extends AllPlaylistFragment implements DialogInterface.OnCancelListener, DialogInterface.OnDismissListener{

    private static final String TAG = App.TAG + "." + AddToPlaylistDialog.class.getSimpleName();
    public static final String EXTRA_ITEM = "EXTRA_ITEM";

    private View rootView;

    private MusicData.Item mItem;

    int mBackStackId = -1;

    Dialog mDialog;
    boolean mViewDestroyed;
    boolean mDismissed;
    boolean mShownByMe;

    public static AddToPlaylistDialog newInstance(MusicData.Item item) {

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_ITEM, item);
        AddToPlaylistDialog fragment = new AddToPlaylistDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        if (savedInstanceState == null) {
            mItem = getArguments().getParcelable(EXTRA_ITEM);
        } else {
            mItem = savedInstanceState.getParcelable(EXTRA_ITEM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.layout_playlist_dialog, container, false);

        adjustLayout(rootView, playlistArray);

        return rootView;
    }

    private void adjustLayout(View rootView, List<PlaylistItem> data) {
        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.dialog_playlist);

        LayoutInflater inflater = LayoutInflater.from(rootView.getContext());

        View headerView = inflater.inflate(R.layout.layout_dialog_create_playlist, linearLayout, false);

        TextView name;
        TextView count;

        linearLayout.addView(headerView);

        headerView.findViewById(R.id.create_playlist_btn).setOnClickListener(this);

        for (PlaylistItem item : data){
            View listItem = inflater.inflate(R.layout.layout_dialog_playlist_item, linearLayout, false);

            listItem.setTag(item);

            name = (TextView) listItem.findViewById(R.id.dialog_playlist_name);
            count = (TextView) listItem.findViewById(R.id.dialog_playlist_number);

            name.setText(item.getName());
            count.setText(String.valueOf(item.getMusicList().size()));

            linearLayout.addView(listItem);

            listItem.setOnClickListener(this);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(TAG, "onActivityCreated: ");

        mDialog = onCreateDialog(savedInstanceState);
        mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        mDialog.setOnCancelListener(this);
        mDialog.setOnDismissListener(this);

        View view = getView();
        if (view != null) {
            if (view.getParent() != null) {
                throw new IllegalStateException("DialogFragment can not be attached to a container view");
            }
            mDialog.setContentView(view);
        }
        mDialog.setOwnerActivity(getActivity());
        mDialog.setCancelable(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        if (mDialog != null) {
            mViewDestroyed = false;
            mDialog.show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mDialog != null) {
            mDialog.hide();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mDialog != null) {
            // Set removed here because this dismissal is just to hide
            // the dialog -- we don't want this to cause the fragment to
            // actually be removed.
            mViewDestroyed = true;
            mDialog.dismiss();
            mDialog = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(EXTRA_ITEM, mItem);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.dialog_item:
                PlaylistItem item = (PlaylistItem) v.getTag();
                String uriTable = PlaylistInfoProvider.MY_PLAYLIST_CONTENT_URI.toString()
                        + "/" + item.getTableName();
                dbManager.insert(Uri.parse(uriTable), mItem);
                dismissInternal(false);
                break;
            case R.id.create_playlist_btn:
                showDialog(getString(R.string.allPlaylists_title), getString(R.string.allPlaylists_dialog_hint));
                break;
        }

    }

    @Override
    public void onCreateClicked(String newText) {

        hasChanged = true;

        PlaylistItem createdPlaylist = new PlaylistItem(newText);

        saveInSystem(createdPlaylist);
        adjustLayout(rootView, playlistArray);
    }

    protected void saveInSystem(PlaylistItem createdPlaylist) {
        dbManager.createTable(createdPlaylist.getTableName());
        playlistArray.add(createdPlaylist);
    }

    public void dismiss() {
        dismissInternal(false);
    }

    public int show(FragmentTransaction transaction, String tag) {
        mDismissed = false;
        mShownByMe = true;
        transaction.add(this, tag);
        mViewDestroyed = false;
        mBackStackId = transaction.commit();
        return mBackStackId;
    }

    private Dialog onCreateDialog(Bundle savedInstanceState) {
        int theme = Resources.getSystem().getIdentifier("Theme_DeviceDefault_Dialog_NoFrame", "style", "android");

        return new Dialog(getActivity(), theme);
    }

    private void dismissInternal(boolean allowStateLoss) {
        if (mDismissed) {
            return;
        }
        mDismissed = true;
        mShownByMe = false;
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
        mViewDestroyed = true;
        if (mBackStackId >= 0) {
            getFragmentManager().popBackStack(mBackStackId,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
            mBackStackId = -1;
        } else {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.remove(this);
            if (allowStateLoss) {
                ft.commitAllowingStateLoss();
            } else {
                ft.commit();
            }
        }
    }


    @Override
    public void onCancel(DialogInterface dialog) {

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (!mViewDestroyed) {

            dismissInternal(true);
        }
    }

}
