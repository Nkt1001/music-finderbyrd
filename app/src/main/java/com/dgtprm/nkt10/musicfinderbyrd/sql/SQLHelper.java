package com.dgtprm.nkt10.musicfinderbyrd.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.sql.SavedAudioReaderContract.PlaylistReaderEntry;


public class SQLHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME  = "AudioReader.db";

    public static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_ENTRIES = "create table " + PlaylistReaderEntry.TABLE_NAME
    + "("
            + PlaylistReaderEntry._ID + " integer primary key autoincrement, "
            + PlaylistReaderEntry.DB_AUDIO_ID + " integer, "
            + PlaylistReaderEntry.DB_NAME + " text, "
            + PlaylistReaderEntry.DB_ARTIST + " text, "
            + PlaylistReaderEntry.DB_DURATION + " integer, "
            + PlaylistReaderEntry.DB_PATH_URI + " text, "
            + PlaylistReaderEntry.DB_PATH_URL + " text, "
            + PlaylistReaderEntry.DB_PLAYLIST + " text"
            + ");";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + PlaylistReaderEntry.TABLE_NAME;

    private static final String SQL_CREATE_BASE_ENTRIES = "create table " + BasePlaylistContract.BasePlaylistEntry.TABLE_NAME
            + "("
            + BasePlaylistContract.BasePlaylistEntry._ID + " integer primary key autoincrement, "
            + BasePlaylistContract.BasePlaylistEntry.DB_AUDIO_ID + " integer, "
            + BasePlaylistContract.BasePlaylistEntry.DB_NAME + " text, "
            + BasePlaylistContract.BasePlaylistEntry.DB_ARTIST + " text, "
            + BasePlaylistContract.BasePlaylistEntry.DB_DURATION + " integer, "
            + BasePlaylistContract.BasePlaylistEntry.DB_PATH_URI + " text, "
            + BasePlaylistContract.BasePlaylistEntry.DB_PATH_URL + " text"
            + ");";

    private static final String SQL_DELETE_BASE_ENTRIES =
            "DROP TABLE IF EXISTS " + BasePlaylistContract.BasePlaylistEntry.TABLE_NAME;
    private static final String TAG = App.TAG + "." + BasePlaylistSQLHelper.class.getSimpleName();


    public SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        db.execSQL(SQL_CREATE_BASE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_DELETE_BASE_ENTRIES);
        onCreate(db);
    }
}
