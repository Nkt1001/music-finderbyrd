package com.dgtprm.nkt10.musicfinderbyrd.util.player;

public interface PlayerInterface {
    void playNext();
    void playPrevious();
    void seekForward();
    void seekBackward();
    void setShuffleMode(boolean what);
    void setRepeatMode(boolean what);
    void pauseMusic();
    void resumeMusic();
}
