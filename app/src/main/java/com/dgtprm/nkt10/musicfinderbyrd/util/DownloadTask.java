package com.dgtprm.nkt10.musicfinderbyrd.util;

import android.content.Context;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadTask extends AsyncTask<MusicData.Item, Integer, String> {

    private final static String TAG = App.TAG + "." + DownloadTask.class.getSimpleName();

    private Context mContext;
    private MusicData.Item mItem;
    private PowerManager.WakeLock mWakeLock;
    private String filePath = "";
    int mProgress = 0;

    public DownloadTask(Context context){
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download

        PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        mWakeLock.acquire();
    }

    @Override
    protected String doInBackground(MusicData.Item... params) {

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;

        try{
            mItem = params[0];
            String urlStr = mItem.getUrl();

            String mUrlStr = urlStr.substring(0, (urlStr.indexOf('?')));
            String fileName = mUrlStr.substring(mUrlStr.lastIndexOf('/'));
            Log.d(TAG, "doInBackground: urlStr = " + urlStr);
            Log.d(TAG, "doInBackground: mUrlStr = " + mUrlStr);
            Log.d(TAG, "doInBackground: fileName = " + fileName);

            URL url = new URL(mUrlStr);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if(connection.getResponseCode() != HttpURLConnection.HTTP_OK){

                return "Server returned HTTP " + connection.getResponseCode() +
                        " " + connection.getResponseMessage();
            }

            int fileLength = connection.getContentLength();


            //download the file
            input = connection.getInputStream();
            filePath = App.getmPath() + fileName;
            output = new FileOutputStream(filePath);

            byte data[] = new byte[4096];
            long total = 0;
            int count;

            while ( (count = input.read(data)) != -1 ){
                if(isCancelled()){
                    input.close();
                    return null;
                }
                total += count;
                //publishing progress
                //only if total length is known
                if(fileLength > 0)
                    publishProgress((int) (total * 100 / fileLength));

                output.write(data, 0, count);
            }

        } catch (Exception e){
            return e.toString();
        } finally {
            try {
                if(output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }
            if (connection != null)
                connection.disconnect();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        int currentProgress = values[0];
        if((currentProgress-mProgress) >= 5) {
            mProgress = currentProgress;
            DownloadingListHolder.getInstance().changeProgress(mItem, mProgress);
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        Log.d(TAG, "onPostExecute: ");
        mWakeLock.release();
//        mPb.setVisibility(View.GONE);
        if(s != null) {
            Toast.makeText(mContext, "Download error " + s, Toast.LENGTH_LONG).show();
            DownloadingListHolder.getInstance().downloadingFailed(mItem);
        }
        else {
            mItem.setUri(filePath);
            DownloadingListHolder.getInstance().downloadingFinished(mItem);
            DownloadedListHolder.getInstance().addNewDownloaded(mItem);
            Toast.makeText(mContext, "File downloaded", Toast.LENGTH_SHORT).show();
        }
    }
}
