package com.dgtprm.nkt10.musicfinderbyrd.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;

public class BasePlaylistSQLHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_ENTRIES = "create table " + BasePlaylistContract.BasePlaylistEntry.TABLE_NAME
            + "("
            + BasePlaylistContract.BasePlaylistEntry._ID + " integer primary key autoincrement, "
            + BasePlaylistContract.BasePlaylistEntry.DB_AUDIO_ID + " integer, "
            + BasePlaylistContract.BasePlaylistEntry.DB_NAME + " text, "
            + BasePlaylistContract.BasePlaylistEntry.DB_ARTIST + " text, "
            + BasePlaylistContract.BasePlaylistEntry.DB_DURATION + " integer, "
            + BasePlaylistContract.BasePlaylistEntry.DB_PATH_URI + " text, "
            + BasePlaylistContract.BasePlaylistEntry.DB_PATH_URL + " text, "
            + ");";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + BasePlaylistContract.BasePlaylistEntry.TABLE_NAME;
    private static final String TAG = App.TAG + "." + BasePlaylistSQLHelper.class.getSimpleName();

    public BasePlaylistSQLHelper(Context context) {
        super(context, SQLHelper.DATABASE_NAME, null, SQLHelper.DATABASE_VERSION);
        Log.d(TAG, "BasePlaylistSQLHelper: ");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: ");
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
