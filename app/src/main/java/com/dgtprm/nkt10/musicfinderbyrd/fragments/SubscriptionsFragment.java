package com.dgtprm.nkt10.musicfinderbyrd.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.adapters.MysubscribeRecyclerViewAdapter;
import com.dgtprm.nkt10.musicfinderbyrd.model.SubscriptionsObject;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnItemClickListener}
 * interface.
 */
public class SubscriptionsFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_NAME = "name";
    private static final String TAG = App.TAG + "." + SubscriptionsFragment.class.getSimpleName();

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    private SubscriptionsObject data;

    private String fragmentName = "";

    private OnItemClickListener mListener;

    private Tracker mTracker;

    VKRequest.VKRequestListener requestListener = new VKRequest.VKRequestListener() {
        @Override
        public void onComplete(VKResponse response) {

            turnOffProgress();
            Log.d(TAG, "onComplete: " + response.responseString);
            data = new Gson().fromJson(response.responseString, SubscriptionsObject.class);

            if(null != recyclerView) recyclerView.setAdapter(new MysubscribeRecyclerViewAdapter(data.getResponse().getItems(), mListener, getActivity()));
            super.onComplete(response);
        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
        }

        @Override
        public void onError(VKError error) {

//            Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
            super.onError(error);
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }
    };

    public SubscriptionsFragment() {

    }

    // TODO: Customize parameter initialization
    public static SubscriptionsFragment newInstance(String name) {
        SubscriptionsFragment fragment = new SubscriptionsFragment();
        Bundle args = new Bundle();

        args.putString(ARG_NAME, name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App app = (App) getActivity().getApplication();
        mTracker = app.getDefaultTracker();

        if (getArguments() != null) {
            fragmentName = getArguments().getString(ARG_NAME);

            progressBar = (ProgressBar) getActivity().findViewById(R.id.main_progressBar);
            turnOnProgress();
            if (fragmentName.equals(getString(R.string.tab_friends))){
                VKApi.friends().get(VKParameters.from("order", "hints", "fields", "nickname", "fields", "photo_50")).executeWithListener(requestListener);
            }else if (fragmentName.equals(getString(R.string.tab_groups))){
                VKApi.groups().get(VKParameters.from("extended", 1)).executeWithListener(requestListener);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscribe_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;

            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            if(null != data) recyclerView.setAdapter(new MysubscribeRecyclerViewAdapter(data.getResponse().getItems(), mListener,  getActivity()));
        }
        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnItemClickListener) {
            mListener = (OnItemClickListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

   private void turnOnProgress(){
       progressBar.setVisibility(View.VISIBLE);
   }

    private void turnOffProgress(){
        progressBar.setVisibility(View.GONE);
    }

    public interface OnItemClickListener {
        // TODO: Update argument type and name
        void onItemClick(SubscriptionsObject.Item item);
    }
}
