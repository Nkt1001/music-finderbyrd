package com.dgtprm.nkt10.musicfinderbyrd.model.system;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class API_ID implements Parcelable {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("response")
    @Expose
    public Response response;

    protected API_ID(Parcel in) {
        response = in.readParcelable(Response.class.getClassLoader());
    }

    public static final Creator<API_ID> CREATOR = new Creator<API_ID>() {
        @Override
        public API_ID createFromParcel(Parcel in) {
            return new API_ID(in);
        }

        @Override
        public API_ID[] newArray(int size) {
            return new API_ID[size];
        }
    };

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(response, flags);
    }

    @Override
    public String toString() {
        return getResponse().getVkApiId().toString();
    }

    public static class Response implements Parcelable {
        @SerializedName("vk-api-id")
        @Expose
        public Integer vkApiId;

        protected Response(Parcel in) {
            vkApiId = in.readInt();
        }

        public static final Creator<Response> CREATOR = new Creator<Response>() {
            @Override
            public Response createFromParcel(Parcel in) {
                return new Response(in);
            }

            @Override
            public Response[] newArray(int size) {
                return new Response[size];
            }
        };

        public Integer getVkApiId() {
            return vkApiId;
        }

        public void setVkApiId(Integer vkApiId) {
            this.vkApiId = vkApiId;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(vkApiId);
        }
    }
}
