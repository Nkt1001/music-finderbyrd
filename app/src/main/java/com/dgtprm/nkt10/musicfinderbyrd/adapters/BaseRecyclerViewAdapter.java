package com.dgtprm.nkt10.musicfinderbyrd.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.BasePlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.util.SecondsToMinutesConverter;

import java.util.List;
import java.util.Observer;

public abstract class BaseRecyclerViewAdapter extends RecyclerView.Adapter<BaseRecyclerViewAdapter.ViewHolder> implements Observer {

    private static final String TAG = App.TAG + "." + BaseRecyclerViewAdapter.class.getSimpleName();
    protected List<MusicData.Item> mValues;
    protected final BasePlaylistFragment.PlaylistFragmentInteractionListener mListener;

    public BaseRecyclerViewAdapter(List<MusicData.Item> items,
                                   BasePlaylistFragment.PlaylistFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;

        subscribeOnObservables();
    }

    protected abstract void subscribeOnObservables();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_playlistitem, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (mValues.size() == 0 || mValues.get(position).getId() == null) return;

        bind(holder, position);
    }

    private void bind(final ViewHolder holder, int position){
        holder.mItem = mValues.get(position);

        Log.d(TAG, "onBindViewHolder: item = " + holder.mItem.getTitle());
        Log.d(TAG, "onBindViewHolder: state = " + holder.mItem.getStateHolder());
        holder.extraLayout.setVisibility(holder.mItem.getStateHolder().getExtraMenuVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

        bindPlayButton(holder);

        holder.loadingButton.setBackgroundResource(holder.mItem.getStateHolder().getLoadingButtonRes());

        holder.downloadedSticker.setImageResource(holder.mItem.getStateHolder().getStickerRes());
        holder.downloadedSticker.setVisibility(holder.mItem.getStateHolder().getStickerVisibility() == View.VISIBLE ? View.VISIBLE : View.INVISIBLE);


        holder.extraMoreImage.setImageResource(holder.mItem.getStateHolder().getExtraMenuIconRes());

        holder.songName.setText(holder.mItem.getTitle());
        holder.artistName.setText(holder.mItem.getArtist());
        holder.songDuration.setText(SecondsToMinutesConverter.convert(holder.mItem.getDuration()));

        setListeners(holder);
    }

    protected abstract void bindPlayButton(ViewHolder holder);

    protected abstract void setListeners(final ViewHolder holder);

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final View orangeLine;

        public final TextView songName;
        public final TextView artistName;
        public final TextView songDuration;

        public final ImageView downloadedSticker;
        public final ImageView playStopImage;
        public final ImageView extraMoreImage;

        public final FrameLayout extraButtonLayout;
        public final FrameLayout playButtonLayout;

        public final ProgressBar progressBar;

        public final LinearLayout extraLayout;

        public final FrameLayout loadingButton;

        public MusicData.Item mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            songName = (TextView) view.findViewById(R.id.playlist_item_name);
            artistName = (TextView) view.findViewById(R.id.playlist_item_artist);
            songDuration = (TextView) view.findViewById(R.id.playlist_item_timer);

            orangeLine = view.findViewById(R.id.playlist_item_stripe);

            downloadedSticker = (ImageView) view.findViewById(R.id.playlist_item_extra_sticker);
            playStopImage = (ImageView) view.findViewById(R.id.playlist_item_play);
            extraMoreImage = (ImageView) view.findViewById(R.id.playlist_item_extra_icon);

            extraButtonLayout = (FrameLayout) view.findViewById(R.id.playlist_item_extraMenu);
            playButtonLayout = (FrameLayout) view.findViewById(R.id.playlist_item_play_layout);

            extraLayout = (LinearLayout) view.findViewById(R.id.playlist_item_extra);

            progressBar = (ProgressBar) view.findViewById(R.id.playlist_item_progress);

            loadingButton = (FrameLayout) view.findViewById(R.id.playlist_item_extra_download);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + songName.getText() + "'";
        }
    }
}

