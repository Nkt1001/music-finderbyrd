package com.dgtprm.nkt10.musicfinderbyrd.sql;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.sql.SavedAudioReaderContract.PlaylistReaderEntry;


public class PlaylistInfoProvider extends ContentProvider {

    private static final String TAG = App.TAG + "." + PlaylistInfoProvider.class.getSimpleName();

    static final String AUTHORITY = "com.dgtprm.nkt10.musicfinderbyrd.provider";

    static final String AUDIO_PATH = "playlists";

    public static final String BASE_PLAYLIST_AUDIO_PATH = "/base";

    public static final Uri PLAYLIST_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + AUDIO_PATH);

    public static final Uri MY_PLAYLIST_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + AUDIO_PATH + BASE_PLAYLIST_AUDIO_PATH);

    // Типы данных
    // набор строк
    static final String AUDIO_PLAYLIST_TYPE = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + AUDIO_PATH;

    // одна строка
    static final String AUDIO_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY + "." + AUDIO_PATH;

    static final int URI_ALL = 0;
    static final int URI_ITEM = 1;
    static final int URI_PLAYLIST = 2;
    static final int URI_BASE_PLAYLIST = 3;

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, AUDIO_PATH, URI_ALL);
//        uriMatcher.addURI(AUTHORITY, AUDIO_PATH + "/*", URI_PLAYLIST);
        uriMatcher.addURI(AUTHORITY, AUDIO_PATH + "/#", URI_ITEM);
        uriMatcher.addURI(AUTHORITY, AUDIO_PATH + BASE_PLAYLIST_AUDIO_PATH, URI_BASE_PLAYLIST);
        uriMatcher.addURI(AUTHORITY, AUDIO_PATH + BASE_PLAYLIST_AUDIO_PATH + "/*", URI_PLAYLIST);
    }

    SQLHelper helper;
    SQLiteDatabase database;

    @Override
    public boolean onCreate() {
        helper = new SQLHelper(getContext());
        return true;
    }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        String tableName;
        Log.d(TAG, "query: ");
        switch (uriMatcher.match(uri)){
            case URI_ALL:
                Log.d(TAG, "query: ALL");
                tableName = PlaylistReaderEntry.TABLE_NAME;
                break;
            case URI_ITEM:
                Log.d(TAG, "query: ITEM ID " + uri.getLastPathSegment());
                tableName = PlaylistReaderEntry.TABLE_NAME;
                selection =
                        PlaylistReaderEntry.DB_AUDIO_ID + " = " + uri.getLastPathSegment();
                break;
            case URI_PLAYLIST:
//                tableName = uri.getLastPathSegment();
                tableName = "'" + uri.getLastPathSegment() + "'";
                Log.d(TAG, "query: playlist = " + tableName);
                break;
            case URI_BASE_PLAYLIST:
                tableName = BasePlaylistContract.BasePlaylistEntry.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Wrong URI " + uri);
        }

        database = helper.getWritableDatabase();

        Cursor c = database
                .query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), PLAYLIST_CONTENT_URI);

        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)){
            case URI_ALL:
            case URI_PLAYLIST:
            return AUDIO_PLAYLIST_TYPE;
            case URI_ITEM:
                return AUDIO_ITEM_TYPE;
        }
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        String tableName;
        Log.d(TAG, "query: ");
        switch (uriMatcher.match(uri)){

            case URI_ALL:
                Log.d(TAG, "query: ALL");
                tableName = PlaylistReaderEntry.TABLE_NAME;
                break;

            case URI_BASE_PLAYLIST:
                tableName = BasePlaylistContract.BasePlaylistEntry.TABLE_NAME;
                break;

            case URI_PLAYLIST:
                tableName = "'" + uri.getLastPathSegment() + "'";
                break;

            default:
                throw new IllegalArgumentException("Wrong URI " + uri);
        }


        database = helper.getWritableDatabase();
        long rowId = database.insert(tableName, null, values);
        Uri resultUri = ContentUris.withAppendedId(uri, rowId);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        Log.d(TAG, "delete: ");
        String tableName;

        switch (uriMatcher.match(uri)){
            case URI_ALL:
                tableName = PlaylistReaderEntry.TABLE_NAME;
                Log.d(TAG, "delete: ALL");
                break;

            case URI_ITEM:
                Log.d(TAG, "delete: ITEM ID " + uri.getLastPathSegment());
                tableName = PlaylistReaderEntry.TABLE_NAME;

                selection =
                            PlaylistReaderEntry.DB_AUDIO_ID + " = " + uri.getLastPathSegment();
                break;

            case URI_PLAYLIST:
                tableName = "'" + uri.getLastPathSegment() + "'";
                Log.d(TAG, "delete: " + tableName);
                break;

            case URI_BASE_PLAYLIST:
                tableName = BasePlaylistContract.BasePlaylistEntry.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Wrong URI " + uri);
        }

        database = helper.getWritableDatabase();
        int cnt = database.delete(tableName, selection, selectionArgs);
        Log.d(TAG, "delete: selection = " + selection);
        Log.d(TAG, "delete: cnt  = " + cnt);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        switch (uriMatcher.match(uri)){
            case URI_ALL:
                Log.d(TAG, "query: ALL");
                break;
            case URI_ITEM:
                Log.d(TAG, "query: ITEM ID " + uri.getLastPathSegment());
                selection =
                        PlaylistReaderEntry.DB_AUDIO_ID + " = " + uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("Wrong URI " + uri);
        }

        database = helper.getWritableDatabase();
        int cnt = database.update(PlaylistReaderEntry.TABLE_NAME, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }
}
