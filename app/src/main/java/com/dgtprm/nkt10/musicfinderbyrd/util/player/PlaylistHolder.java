package com.dgtprm.nkt10.musicfinderbyrd.util.player;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.sql.DBMusicManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

public class PlaylistHolder extends Observable {
    private static final String TAG = App.TAG + "." + PlaylistHolder.class.getSimpleName();
    private static PlaylistHolder playlistHolder = null;

    private List<MusicData.Item> initializedPlaylist;
    private List<MusicData.Item> playlist;

    private int currentPlaying = -1; // nothing is playing
    private MusicData.Item itemPlaying;
    private MusicData.Item lastPlaying;

    private boolean shuffled;

    HashMap<PlayerEvents, MusicData.Item> eventsMap;

    private PlaylistHolder(List<MusicData.Item> playlist){
        this.initializedPlaylist = playlist;
        makePlaylist();
        Log.d(TAG, "PlaylistHolder: ");
    }

    public synchronized static PlaylistHolder newInstance(List<MusicData.Item> playlist){

        if(playlistHolder == null) {
            playlistHolder = new PlaylistHolder(playlist);
        }else{
            playlistHolder.setPlaylist(playlist);
        }

        return playlistHolder;
    }


    public static PlaylistHolder getInstance(){
        return playlistHolder;
    }

    public synchronized void addInPlaylist(List<MusicData.Item> addition){
        this.initializedPlaylist.addAll(addition);

        makePlaylist();
    }

    private void makePlaylist(){
        if (initializedPlaylist == null) return;

        playlist = new ArrayList<>(initializedPlaylist);
        if (shuffled){
            Collections.shuffle(playlist);
        }
    }

    public synchronized List<MusicData.Item> getPlaylist() {
        return playlist;
    }

    public synchronized void setPlaylist(List<MusicData.Item> playlist) {
        this.initializedPlaylist = playlist;

        makePlaylist();
    }

    public synchronized MusicData.Item getItemPlaying() {
        return itemPlaying;
    }

    public synchronized void setItemPlaying(MusicData.Item itemPlaying) {

        if(this.currentPlaying != -1) {
//            eventsMap = new HashMap<>(2);
//            eventsMap.put(PlayerEvents.PLAYER_PREVIOUS, this.itemPlaying);
//            eventsMap.put(PlayerEvents.PLAYER_NEXT, itemPlaying);
            setStopped();
        }

        eventsMap = new HashMap<>(1);
        eventsMap.put(PlayerEvents.PLAYER_PLAY, itemPlaying);

        this.itemPlaying = itemPlaying;

        currentPlaying = playlist.indexOf(itemPlaying);

        lastPlaying = new MusicData.Item(itemPlaying);

        setChanged();
        notifyObservers(eventsMap);
        Log.d(TAG, "setItemPlaying: ");
    }

    public synchronized MusicData.Item getLastPlaying() {
        return lastPlaying;
    }

    public synchronized void setLastPlaying(MusicData.Item lastPlaying) {
        this.lastPlaying = lastPlaying;
    }

    public synchronized int getCurrentPlaying() {
        return currentPlaying;
    }

//    public synchronized void setCurrentPlaying(int currentPlaying) {
//
//        if (currentPlaying < playlist.size()) {
//            if (this.currentPlaying != -1) {
//                eventsMap = new HashMap<>(2);
//                eventsMap.put(PlayerEvents.PLAYER_PREVIOUS, playlist.get(currentPlaying));
//                eventsMap.put(PlayerEvents.PLAYER_NEXT, playlist.get(currentPlaying));
//            } else {
//                eventsMap = new HashMap<>(1);
//                eventsMap.put(PlayerEvents.PLAYER_PLAY, playlist.get(currentPlaying));
//            }
//
//            this.currentPlaying = currentPlaying;
//
//            itemPlaying = playlist.get(currentPlaying);
//
//            setChanged();
//            notifyObservers(eventsMap);
//            Log.d(TAG, "setCurrentPlaying: " + currentPlaying);
//        }
//    }

    public synchronized void setShuffleMode(boolean shuffleMode){
        shuffled = shuffleMode;
        makePlaylist();
    }

    public synchronized void removeFromPlaylist(Context ctx, MusicData.Item item, Uri uri){

        if (playlist.contains(item) && getIndex(item) != -1)
            playlist.remove(getIndex(item));

        HashMap<PlayerEvents, MusicData.Item> eventsMap = new HashMap<>(1);
        eventsMap.put(PlayerEvents.PLAYER_REMOVE, item);

        DBMusicManager dbManager = new DBMusicManager(ctx);

        dbManager.deleteItemFromTable(uri, item);
        setChanged();
        notifyObservers(eventsMap);
    }

    public synchronized void setStopped(){
        HashMap<PlayerEvents, MusicData.Item> eventsMap = new HashMap<>(1);
        eventsMap.put(PlayerEvents.PLAYER_STOP, itemPlaying);

        if (itemPlaying != null)
            lastPlaying = new MusicData.Item(itemPlaying);
        currentPlaying = -1;
        itemPlaying = null;

        setChanged();
        notifyObservers(eventsMap);
        Log.d(TAG, "setStopped: ");
    }

    public synchronized void setPaused(){
        HashMap<PlayerEvents, MusicData.Item> eventsMap = new HashMap<>(1);
        eventsMap.put(PlayerEvents.PLAYER_STOP, itemPlaying);

        lastPlaying = new MusicData.Item(itemPlaying);
        currentPlaying = -1;
        itemPlaying = null;

        setChanged();
        notifyObservers(eventsMap);
        Log.d(TAG, "setStopped: ");
    }

    private int getIndex(MusicData.Item item) {
        int index=-1;
        for (int i = 0; i < playlist.size(); i++){
            if (playlist.get(i).get_id() == item.get_id()) {
                index = i;
                return index;
            }
        }
        return index;
    }
}
