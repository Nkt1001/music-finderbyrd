package com.dgtprm.nkt10.musicfinderbyrd;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.adapters.BaseRecyclerViewAdapter;
import com.dgtprm.nkt10.musicfinderbyrd.animations.MyAnimator;
import com.dgtprm.nkt10.musicfinderbyrd.dialogs.AddToPlaylistDialog;
import com.dgtprm.nkt10.musicfinderbyrd.dialogs.MusicInfoDialog;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.AllPlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.BasePlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.DownloadedMusicFragment;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.MyPlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.SettingsFragment;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.SubscriptionsFragment;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.VkPlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.model.GlobalUser;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.PlaylistItem;
import com.dgtprm.nkt10.musicfinderbyrd.model.SubscriptionsObject;
import com.dgtprm.nkt10.musicfinderbyrd.service.MediaService;
import com.dgtprm.nkt10.musicfinderbyrd.util.CircleTransform;
import com.dgtprm.nkt10.musicfinderbyrd.util.DownloadTask;
import com.dgtprm.nkt10.musicfinderbyrd.util.MyViewPagerAdapter;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlayerEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;
import com.vk.sdk.VKSdk;

import java.lang.reflect.Field;
import java.util.List;

import es.claucookie.miniequalizerlibrary.EqualizerView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BasePlaylistFragment.PlaylistFragmentInteractionListener,
        SubscriptionsFragment.OnItemClickListener, AllPlaylistFragment.PlaylistSelectedListener {

    private static final String TAG = App.TAG + "." + MainActivity.class.getSimpleName();

    private static final String MY_PLAYLIST_TAG = "my_playlist";
    private static final String FRIEND_PLAYLIST_TAG = "friend_playlist";

    private static final int NOTIFICATION_ID = -999;

    private AppBarLayout appBarLayout;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private FrameLayout fragmentContainer;
    private EqualizerView equalizerView;

    private GlobalUser user;

    private SharedPreferences mPref;

    private Tracker mTracker;

    private App app;

    View.OnClickListener extraItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MusicData.Item item = (MusicData.Item) v.getTag();

            switch (v.getId()){
                case R.id.playlist_item_extra_info:
                    showInfoDialog(item);
                    break;
                case R.id.playlist_item_extra_add:
                    showPlaylistDialog(item);
                    break;
            }
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, "onReceive: ");
            if (intent != null) {

                PlayerEvents event = (PlayerEvents) intent.getSerializableExtra(PlayerActivity.SERVICE_COMMAND);

                switch (event) {
                    case PLAYER_PREPARING:
                        if (!intent.hasExtra(MediaService.EXTRA_IS_PLAYING)) break;

                        if (intent.getBooleanExtra(MediaService.EXTRA_IS_PLAYING, false)){
                            equalizerView.animateBars();
                        }else{
                            equalizerView.stopBars();
                        }
                        break;
                    case PLAYER_PREPARED:
                        if (equalizerView.isAnimating()) break;

                        equalizerView.animateBars();
                        break;
                    case PLAYER_STOP:
                        if (!equalizerView.isAnimating()) break;

                        equalizerView.stopBars();
                        break;
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        if(!checkAuth()){
            startActivity(new Intent(this, LoginActivity.class));
            this.finish();
        }else{

            app = (App) getApplication();
            mTracker = app.getDefaultTracker();

            setContentView(R.layout.activity_main);

            mPref = PreferenceManager.getDefaultSharedPreferences(this);

            if(getIntent().getParcelableExtra(LoginActivity.EXTRA_USER) != null) {
                user = getIntent().getParcelableExtra(LoginActivity.EXTRA_USER);
            }else{
                String firstName = mPref.getString(getString(R.string.pref_firstName), "");
                String lastName = mPref.getString(getString(R.string.pref_lastName), "");
                String avatarUrl = mPref.getString(getString(R.string.pref_avatar_url), "");
                App.setUserId(mPref.getLong(getString(R.string.pref_userId), -1));

                user = GlobalUser.getInstance();

                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setPhoto100(avatarUrl);
                user.setId(App.getUserId());
            }

            Toolbar toolbar = (Toolbar) findViewById(R.id.main_appbar_toolbar);
            setSupportActionBar(toolbar);

            getSupportActionBar().setDisplayShowTitleEnabled(false);

            fragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);

            appBarLayout = (AppBarLayout) findViewById(R.id.main_appbar);
            viewPager = (ViewPager) findViewById(R.id.main_view_pager);
            equalizerView = (EqualizerView) findViewById(R.id.equalizer_view);

            tabLayout = new TabLayout(this);
            adjustTabLayout(tabLayout);

            initFab();

            initDrawer(toolbar);

            getFragmentManager().beginTransaction().add(fragmentContainer.getId(),
                    getPlaylistFragment(R.id.nav_paylists, VkPlaylistFragment.COUNT_ITEMS_ON_PAGE, null)).commit();

            setVolumeControlStream(AudioManager.STREAM_MUSIC);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        registerReceiver(receiver, new IntentFilter(PlayerActivity.ACTION_MUSIC));
        startService(new Intent(this, MediaService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume: ");
        mTracker.setScreenName("Main");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");

        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();

//        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
//                new Intent(getApplicationContext(), PlayerActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
//
//        MusicData.Item item = PlaylistHolder.getInstance().getLastPlaying();
//
//        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext());
//        notification.setSmallIcon(R.mipmap.ic_notif_play)
//                .setContentInfo("Music Finderbyrd")
//                .setContentText(item.getArtist() + " - " + item.getTitle())
//                .;
//
//        Intent resultIntent = new Intent(this, PlayerActivity.class);
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addParentStack(PlayerActivity.class);
//
//        stackBuilder.addNextIntent(resultIntent);
//        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
//        notification.setContentIntent(resultPendingIntent);
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(NOTIFICATION_ID, notification.build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setIconifiedByDefault(false);

        setSearchExpandField(menu);

        searchView.setQueryHint(getString(R.string.action_search));

        adjustSearchCloseButton(searchView);

        adjustSearchTextColor(searchView);

        setSearchListener(searchView);
        return true;
    }

    private void adjustSearchCloseButton(SearchView searchView) {
        try {
            Field searchField = SearchView.class.getDeclaredField("mCloseButton");
            searchField.setAccessible(true);
            ImageView closeBtn = (ImageView) searchField.get(searchView);
            closeBtn.setImageResource(R.drawable.ic_close);


        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void setSearchExpandField(final Menu menu) {
        findViewById(R.id.toolbar_empty_field).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.findItem(R.id.search).expandActionView();
            }
        });
    }

    private void setSearchListener(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() > 2) {
                    Toast.makeText(MainActivity.this, query, Toast.LENGTH_SHORT).show();
                    search(query, VkPlaylistFragment.COUNT_ITEMS_ON_PAGE);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void adjustSearchTextColor(SearchView searchView) {
        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) searchView.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initFab(){
        View eqButton = findViewById(R.id.equalizer_button);
        eqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PlayerActivity.class));
            }
        });
    }

    private void initDrawer(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();

//        String name = GlobalUser.getInstance().getFirstName();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        initUser(navigationView);
//        navigationView.setCheckedItem(mPref.getInt(getString(R.string.pref_navdrawer_selectedItem), R.id.nav_paylists));
    }

    private void adjustTabLayout(TabLayout view){

        view.setTabMode(TabLayout.MODE_FIXED);
        view.setTabGravity(TabLayout.GRAVITY_FILL);
        view.setTabTextColors(ColorStateList.valueOf(ContextCompat.getColor(this, android.R.color.white)));
    }

    private void initUser(NavigationView navigationView){

        View header = navigationView.getHeaderView(0);
        TextView tvUserName = (TextView) header.findViewById(R.id.nav_header_name);
        ImageView avatar = (ImageView) header.findViewById(R.id.nav_header_avatar);

        tvUserName.setText(user.toString());
        Picasso.with(getApplicationContext()).load(user.getPhoto100())
                .transform(new CircleTransform()).placeholder(R.drawable.ic_user_avatar).fit().into(avatar);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(drawer ==null) {

            super.onBackPressed();
            return;
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }

    private boolean checkAuth(){
        return VKSdk.wakeUpSession(this) && PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.access_token_key), false);
    }

    private void search(String query, int count){

        String[] argNames = new String[]{"q", "auto_complete", "offset", "count"};
        String[] argValues = new String[]{query, "1", String.valueOf(0), String.valueOf(count)};
        Bundle b = new Bundle();
        b.putStringArray(BasePlaylistFragment.ARG_PARAMS, getArgPairs(argNames, argValues));
        b.putInt(BasePlaylistFragment.ARG_METHOD, VkPlaylistFragment.ARG_AUDIO_SEARCH);
        changeFragment(VkPlaylistFragment.newInstance(b));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Log.d(TAG, "onNavigationItemSelected: appBarLayout.getCount" + appBarLayout.getChildCount());


        int id = item.getItemId();

        mPref.edit().putInt(getString(R.string.pref_navdrawer_selectedItem), id).apply();

        if (id == R.id.nav_subscriptions) {

            showSubscriptions();
        } else if (id == R.id.nav_downloads) {
            changeFragment(DownloadedMusicFragment.newInstance(null));

        } else if (id == R.id.nav_settings) {

            changeFragment(new SettingsFragment());
        } else if (id == R.id.nav_mPlaylist) {

            changeFragment(new AllPlaylistFragment());
        } else{

            changeFragment(getPlaylistFragment(id, VkPlaylistFragment.COUNT_ITEMS_ON_PAGE, null));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        return true;
    }

    private void showSubscriptions(){

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        appBarLayout.addView(tabLayout);
    }

    private void clearToolbar(){
        if(appBarLayout.getChildCount() > 1)
            appBarLayout.removeView(tabLayout);
    }

    private void setupViewPager(ViewPager viewPager){

        clearToolbar();
        if (fragmentContainer.getVisibility() == View.VISIBLE) {
            fragmentContainer.setVisibility(View.GONE);
            viewPager.setVisibility(View.VISIBLE);
        }
        MyViewPagerAdapter adapter = new MyViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(SubscriptionsFragment.newInstance(getString(R.string.tab_friends)), getString(R.string.tab_friends));
        adapter.addFragment(SubscriptionsFragment.newInstance(getString(R.string.tab_groups)), getString(R.string.tab_groups));

        mTracker.setScreenName("Subsccriptions Fragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        viewPager.setAdapter(adapter);
    }

    private void changeFragment(Fragment fragment){

        if(fragment != null) {

            if(viewPager.getVisibility() == View.VISIBLE) {
                clearToolbar();
                fragmentContainer.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.GONE);
            }

            FragmentTransaction ft = getFragmentManager().beginTransaction();

//            getFragmentManager().getBackStackEntryCount();
//            FragmentManager.BackStackEntry entry = getFragmentManager().getBackStackEntryAt(0);
//            Fragment
            ft.replace(R.id.fragment_container, fragment).commit();
        }
    }

//    private void addFragment(Fragment fragment){
//
//        if (fragment != null){
//
//            if(viewPager.getVisibility() == View.VISIBLE) {
//                clearToolbar();
//                fragmentContainer.setVisibility(View.VISIBLE);
//                viewPager.setVisibility(View.GONE);
//            }
//
//            getFragmentManager().beginTransaction()
//                    .add(R.id.fragment_container, fragment)
//                    .addToBackStack(null)
//                    .commit();
//        }
//    }

    private Fragment getPlaylistFragment(int id, int count, SubscriptionsObject.Item owner){
        Bundle args = new Bundle();
        String[] params = getPlaylistParams(id, count, owner);
        args.putStringArray(BasePlaylistFragment.ARG_PARAMS, params);

        switch (id){
            case R.id.nav_popular:
                args.putInt(BasePlaylistFragment.ARG_METHOD, VkPlaylistFragment.ARG_AUDIO_POPULAR);
                break;
            case R.id.nav_recommends:
                args.putInt(BasePlaylistFragment.ARG_METHOD, VkPlaylistFragment.ARG_AUDIO_RECOM);
                break;

            default:
                args.putInt(BasePlaylistFragment.ARG_METHOD, VkPlaylistFragment.ARG_AUDIO_GET);
                break;

        }
        return VkPlaylistFragment.newInstance(args);
    }

    private void showInfoDialog(MusicData.Item item){
        Log.d(TAG, "showDialog: " + item);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if(prev != null){
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        Bundle b = new Bundle();
        b.putParcelable(getString(R.string.extra_info), item);

        MusicInfoDialog infoDialog = MusicInfoDialog.newInstance(b);
        infoDialog.show(ft, "dialog");
    }

    private void showPlaylistDialog(MusicData.Item item){
        Log.d(TAG, "showPlaylistDialog: ");

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if(prev != null){
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        AddToPlaylistDialog dialog = AddToPlaylistDialog.newInstance(item);
        dialog.show(ft, "dialog");
    }

    private String[] getPlaylistParams(int id, int count, SubscriptionsObject.Item owner){
        String[] argsNames;
        String[] argsValues;

        Log.d(TAG, "getPlaylistParams: ");
        switch (id){
            case R.id.nav_popular:
                Log.d(TAG, "getPlaylistParams: nav_popular");

                argsNames = new String[]{"offset", "count"};
                argsValues = new String[]{String.valueOf(0), String.valueOf(count)};
                break;
            case R.id.nav_recommends:

                Log.d(TAG, "getPlaylistParams: nav_recommends");

                argsNames = new String[]{"user_id", "offset", "count"};
                argsValues = new String[]{String.valueOf(user.getId()), String.valueOf(0), String.valueOf(count)};
                break;
            case R.id.nav_downloads:
                Log.d(TAG, "getPlaylistParams: nav_downloads");

                argsNames = new String[]{"owner_id", "count"};
                argsValues = new String[]{String.valueOf(user.getId()), String.valueOf(count)};
                break;

            default:
                Log.d(TAG, "getPlaylistParams: nav_paylists");

                argsNames = new String[]{"owner_id", "offset", "count"};
                long ownerId;
                if(null != owner){
                    if (null != owner.getFirstName()) {
                        Log.d(TAG, "getPlaylistParams: " + owner.getFirstName());
                        ownerId = owner.getId();
                    }
                    else {
                        Log.d(TAG, "getPlaylistParams: group");
                        ownerId = -owner.getId();
                        Log.d(TAG, "getPlaylistParams: " + ownerId);
                    }
                } else{

                    ownerId = user.getId();
                }

                argsValues = new String[]{String.valueOf(ownerId), String.valueOf(0), String.valueOf(count)};
                break;
        }

        return getArgPairs(argsNames, argsValues);
    }

    private String[] getArgPairs(String[] argName, String[] argValues){
        String[] result = new String[argName.length * 2];

        for(int i = 0; i < result.length; i ++){
            result[i] = i % 2 == 0 ? argName[i/2] : argValues[i/2];
        }
        return result;
    }

    @Override
    public void onPlayButtonPressed(MusicData.Item item, List<MusicData.Item> items, boolean inCustomPlaylist) {

        PlaylistHolder playlistHolder;
        playlistHolder = PlaylistHolder.newInstance(items);

        MusicData.Item itemPlaying = playlistHolder.getItemPlaying();

        if (!inCustomPlaylist) {
            if (itemPlaying != null && playlistHolder.getItemPlaying().equals(item)) {

                MediaService.pauseMediaService(this);
            } else {
                MediaService.startMediaService(this, item);
            }
        } else {
            if (itemPlaying != null && itemPlaying.get_id() == item.get_id() && itemPlaying.equals(item)){
                MediaService.pauseMediaService(this);
            } else {
                Log.d(TAG, "onPlayButtonPressed: item " + item.getTitle());
                Log.d(TAG, "onPlayButtonPressed: item " + item.getStateHolder());
                MediaService.startMediaService(this, item);
            }
        }
    }

    @Override
    public void onExtraButtonPressed(BaseRecyclerViewAdapter.ViewHolder viewHolder) {
        MusicData.Item item = viewHolder.mItem;
        Log.d(TAG, "onExtraButtonPressed: " + item);
        Log.d(TAG, "onExtraButtonPressed: " + item.getStateHolder());
        View extraLayout = viewHolder.extraLayout;
        ImageView icMore = viewHolder.extraMoreImage;

        boolean isVisible = (extraLayout.getVisibility() != View.GONE) && (extraLayout.getVisibility() != View.INVISIBLE);

        if (isVisible) {
            item.getStateHolder().hideExtraMenu();
            new MyAnimator().closeExtraAnimation(icMore);
            extraLayout.setVisibility(View.GONE);
        }
        else {
            item.getStateHolder().showExtraMenu();
            new MyAnimator().openExtraAnimation(icMore);
            extraLayout.setVisibility(View.VISIBLE);

            View infoButton = extraLayout.findViewById(R.id.playlist_item_extra_info);
            View addButton = extraLayout.findViewById(R.id.playlist_item_extra_add);
//            View downloadButton = extraLayout.findViewById(R.id.playlist_item_extra_download);

            adjustButtons(viewHolder.mItem, infoButton, addButton); //, downloadButton);
        }
    }

    @Override
    public void onDownloadRemoveButtonPressed(MusicData.Item item, Uri uriTable, boolean onlyRemove) {

        if (onlyRemove) {
            PlaylistHolder.getInstance().removeFromPlaylist(this, item, uriTable);
            return;
        }

        if (!getResources().getBoolean(R.bool.allow_downloading)
                && !mPref.getBoolean(getString(R.string.pref_permissions), true)) return;

        if (DownloadingListHolder.getInstance().addNewDownloading(item)) {
            new DownloadTask(MainActivity.this).execute(item);
        } else{
            DownloadedListHolder.getInstance().removeItem(item);
        }

    }


    private void adjustButtons(MusicData.Item item, View... buttons){

        for (View v : buttons){
            v.setTag(item);
            v.setOnClickListener(extraItemClickListener);
        }
    }
    @Override
    public void onItemClick(SubscriptionsObject.Item item) {
        Toast.makeText(this, item.getFirstName() + " " + item.getLastName(), Toast.LENGTH_SHORT).show();

        changeFragment(getPlaylistFragment(R.id.nav_paylists, VkPlaylistFragment.COUNT_ITEMS_ON_PAGE, item));
//        changeFragment());
    }

    @Override
    public void playlistSelected(PlaylistItem item) {
        Toast.makeText(MainActivity.this, item.getName(), Toast.LENGTH_SHORT).show();

        changeFragment(MyPlaylistFragment.newInstance(item));
    }

}
