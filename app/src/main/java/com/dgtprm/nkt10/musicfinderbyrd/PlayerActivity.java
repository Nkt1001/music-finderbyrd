package com.dgtprm.nkt10.musicfinderbyrd;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.dialogs.AddToPlaylistDialog;
import com.dgtprm.nkt10.musicfinderbyrd.dialogs.MusicInfoDialog;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.AllPlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.PlayerFragment;
import com.dgtprm.nkt10.musicfinderbyrd.model.GlobalUser;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.PlaylistItem;
import com.dgtprm.nkt10.musicfinderbyrd.service.MediaService;
import com.dgtprm.nkt10.musicfinderbyrd.util.DownloadTask;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

public class PlayerActivity extends AppCompatActivity implements PlayerFragment.OnFragmentInteractionListener, AllPlaylistFragment.PlaylistSelectedListener {

    public static final String ACTION_MUSIC = "com.digitalpromo.filter.music";
//    public static final String ACTION_CHANGE = "com.digitalpromo.filter.change";

    public static final String SERVICE_COMMAND = "SERVICE_COMMAND";
    private static final String TAG = App.TAG + "." + PlayerActivity.class.getSimpleName();

    private static final String SHARE_PARAM_OWNER = "owner_id";
    private static final String SHARE_PARAM_MESSAGE = "message";

    private SharedPreferences mPref;

    private MediaService mService;

    private boolean mBound = false;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected: ");
            MediaService.MyBinder binder = (MediaService.MyBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected: ");
            mBound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mPref = PreferenceManager.getDefaultSharedPreferences(this);

        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.bt_back);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mPref.edit().putBoolean(getString(R.string.pref_inside_player), true).commit();
        Intent intent = new Intent(this, MediaService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        mPref.edit().putBoolean(getString(R.string.pref_inside_player), false).commit();
        if (mBound)
            unbindService(mConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.player_activity_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            case R.id.share:
                share(PlaylistHolder.getInstance().getLastPlaying());
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void share(MusicData.Item itemPlaying) {

        if (itemPlaying == null) return;

        String song = itemPlaying.getArtist() + " - " + itemPlaying.getTitle();
        String url = "http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;

        String message = getString(R.string.post_message_lis) + " " + song + "\n" + getString(R.string.post_message_via) + " " + url;
        VKApi.wall().post(VKParameters.from(SHARE_PARAM_OWNER, GlobalUser.getInstance().getId(), SHARE_PARAM_MESSAGE, message)).executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                Toast.makeText(PlayerActivity.this, "Thanks for sharing", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                Toast.makeText(PlayerActivity.this, "Sharing failed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void playNext() {
        mService.playNext();
    }

    @Override
    public void playPrevious() {
        mService.playPrevious();
    }

    @Override
    public void seekForward() {
        mService.seekForward();
    }

    @Override
    public void seekBackward() {
        mService.seekBack();
    }

    @Override
    public void setShuffleMode(boolean what) {
        mService.setShuffleMode(what);
    }

    @Override
    public void setRepeatMode(boolean what) {
        mService.setRepeatMode(what);
    }

    @Override
    public void pauseMusic() {
        if (mService == null) return;

        mService.pauseMusic();
    }

    @Override
    public void resumeMusic() {
        mService.resumeMusic();
    }

    @Override
    public void showInfo(MusicData.Item mItem) {
        Log.d(TAG, "showDialog: " + mItem);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if(prev != null){
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        Bundle b = new Bundle();
        b.putParcelable(getString(R.string.extra_info), mItem);

        MusicInfoDialog infoDialog = MusicInfoDialog.newInstance(b);
        infoDialog.show(ft, "dialog");
    }

    @Override
    public void showPlaylistDialog(MusicData.Item item){
        Log.d(TAG, "showPlaylistDialog: ");

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if(prev != null){
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        AddToPlaylistDialog dialog = AddToPlaylistDialog.newInstance(item);
        dialog.show(ft, "dialog");
    }

    @Override
    public void downloadTrack(MusicData.Item mItem) {
        if (!getResources().getBoolean(R.bool.allow_downloading)
                && !mPref.getBoolean(getString(R.string.pref_permissions), true)) return;


        if (DownloadingListHolder.getInstance().addNewDownloading(mItem)) {
            new DownloadTask(this).execute(mItem);
        } else{
            DownloadedListHolder.getInstance().removeItem(mItem);
        }
    }

    @Override
    public void playlistSelected(PlaylistItem item) {

    }
}
