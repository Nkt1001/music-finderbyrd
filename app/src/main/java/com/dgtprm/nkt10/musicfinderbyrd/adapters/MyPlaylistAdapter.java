package com.dgtprm.nkt10.musicfinderbyrd.adapters;

import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.fragments.BasePlaylistFragment;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicItemStateHolder;
import com.dgtprm.nkt10.musicfinderbyrd.model.PlaylistItem;
import com.dgtprm.nkt10.musicfinderbyrd.sql.PlaylistInfoProvider;
import com.dgtprm.nkt10.musicfinderbyrd.util.LoadingEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlayerEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

public class MyPlaylistAdapter extends BaseRecyclerViewAdapter {
    private static final String TAG = App.TAG + "." + MyPlaylistAdapter.class.getSimpleName();

    private PlaylistItem mPlaylist;

    public MyPlaylistAdapter(List<MusicData.Item> items, BasePlaylistFragment.PlaylistFragmentInteractionListener listener, PlaylistItem playlistItem) {
        super(items, listener);

        mPlaylist = playlistItem;
    }

    @Override
    protected void subscribeOnObservables()  {
        if (PlaylistHolder.getInstance() == null) {
            PlaylistHolder.newInstance(new ArrayList<MusicData.Item>());
        }
        PlaylistHolder.getInstance().addObserver(this);
        DownloadingListHolder.getInstance().addObserver(this);
//        DownloadedListHolder.getInstance().addObserver(this);
    }

    @Override
    protected void bindPlayButton(ViewHolder holder) {

        MusicData.Item playingItem = PlaylistHolder.getInstance().getItemPlaying();

        holder.playStopImage.setImageResource(checkPlaying(holder.mItem, playingItem)
                ? R.drawable.ic_pause_ : R.drawable.bt_track_play);
        holder.orangeLine.setVisibility(checkPlaying(holder.mItem, playingItem)
                ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    protected void setListeners(final ViewHolder holder) {
        holder.extraButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ");
                if (null != mListener) {
                    mListener.onExtraButtonPressed(holder);

                }
            }
        });

        holder.playButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mListener) {
                    mListener.onPlayButtonPressed(holder.mItem, mValues, true);

                }
            }
        });

        holder.loadingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mListener) {

                    String uri = PlaylistInfoProvider.MY_PLAYLIST_CONTENT_URI.toString() + "/" + mPlaylist.getTableName();

                    mListener.onDownloadRemoveButtonPressed(holder.mItem, Uri.parse(uri), true);
                }
            }
        });
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof PlaylistHolder) {
            Log.d(TAG, "update: PlaylistHolder");

            HashMap<PlayerEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>) ?
                    (HashMap<PlayerEvents, MusicData.Item>) data : null;

            if (eventsMap != null) {

                Map.Entry<PlayerEvents, MusicData.Item> entry = eventsMap.entrySet().iterator().next();
                MusicData.Item item = entry.getValue();
                int index = getIndex(item);

                if (mValues.contains(item)){
                    switch (entry.getKey()){
                        case PLAYER_STOP:
                            if (index != -1)
                            notifyItemChanged(index);
                            break;
                        case PLAYER_PLAY:
                            if (index != -1)
                                notifyItemChanged(index);
                            break;
                        case PLAYER_REMOVE:
                            if (index != -1){
                                mValues.remove(index);
                                notifyItemRemoved(index);
                            }
                            break;
                    }
                }
            }
        }

        if (observable instanceof DownloadingListHolder) {

            Log.d(TAG, "update: DownloadingListHolder");

            HashMap<LoadingEvents, MusicData.Item> eventsMap = (data instanceof HashMap<?, ?>) ? (HashMap<LoadingEvents, MusicData.Item>) data : null;

            if (eventsMap != null) {

                Map.Entry<LoadingEvents, MusicData.Item> entry = eventsMap.entrySet().iterator().next();
                MusicData.Item item = entry.getValue();
                int index = getIndex(item);

                if (mValues.contains(item)){
                    switch (entry.getKey()) {
                        case LOADING_NEW:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DOWNLOADING);
                            notifyItemChanged(index);
                            break;
                        case LOADING_FAILED:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DEFAULT);
                            notifyItemChanged(index);
                            break;
                        case LOADING_FINISHED:
                            item.setStateHolder(MusicItemStateHolder.State.STATE_DOWNLOADED);
                            notifyItemChanged(index);
                            break;
                    }
                }
            }
        }
    }

    private boolean checkPlaying(MusicData.Item bindingItem, MusicData.Item playingItem){
        return playingItem != null && mValues.contains(playingItem) && (bindingItem.get_id() == playingItem.get_id());
    }

    private int getIndex(MusicData.Item item) {
        int index=-1;
        for (int i = 0; i < mValues.size(); i++){
            if (mValues.get(i).get_id() == item.get_id())
                return i;
        }
        return index;
    }
}
