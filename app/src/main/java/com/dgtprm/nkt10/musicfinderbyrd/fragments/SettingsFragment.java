package com.dgtprm.nkt10.musicfinderbyrd.fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.LoginActivity;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.dialogs.CreateDialog;
import com.dgtprm.nkt10.musicfinderbyrd.dialogs.CreatePathDialog;
import com.dgtprm.nkt10.musicfinderbyrd.model.GlobalUser;
import com.dgtprm.nkt10.musicfinderbyrd.service.MediaService;
import com.dgtprm.nkt10.musicfinderbyrd.util.ConfigClass;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vk.sdk.VKSdk;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener, CreateDialog.CreateButtonClickListener {

    private static final String TAG = App.TAG + "." + SettingsFragment.class.getSimpleName();

    private View rootView;

    private TextView tvUserName;
    private TextView tvPath;

    private SharedPreferences mPref;
    private String mPath;

    private Tracker mTracker;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        App app = (App) getActivity().getApplication();
        mTracker = app.getDefaultTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        View viewLogout = rootView.findViewById(R.id.settings_logout);
        View viewTerms = rootView.findViewById(R.id.settings_terms);
        View viewRate = rootView.findViewById(R.id.settings_rate);

        tvPath = (TextView) rootView.findViewById(R.id.settings_path);
        tvUserName = (TextView) rootView.findViewById(R.id.settings_userName);

        viewLogout.setOnClickListener(this);
        viewTerms.setOnClickListener(this);
        viewRate.setOnClickListener(this);
        tvPath.setOnClickListener(this);
        tvUserName.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        tvUserName.setText(GlobalUser.getInstance().toString());

        mPath = mPref.getString(getString(R.string.pref_path), "");
        App app = (App) getActivity().getApplication();
        app.createDirIfNotExtists(mPath);


        tvPath.setText(mPath);
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("Settings Fragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void showDialog(String path){
        Log.d(TAG, "showDialog: " + path);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if(prev != null){
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        CreateDialog createPathDialog = CreatePathDialog.newInstance(getString(R.string.settings_set_path_title), path);
        createPathDialog.addCreateClickListener(this);
        createPathDialog.show(ft, "dialog");
    }

    private void rateUpApp() {
        Uri uri = Uri.parse(ConfigClass.ACTION_RATE);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        } else {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        goToMarket.addFlags(flags);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(ConfigClass.URL_RATE)));
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.settings_logout:
                VKSdk.logout();
                MediaService.stopMusicKillService(getActivity());
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
                break;

            case R.id.settings_path:
//                Toast.makeText(getActivity(), "пока недоступно", Toast.LENGTH_SHORT).show();
                showDialog(App.getmPath());
                break;

            case R.id.settings_rate:
                rateUpApp();
                break;

            case R.id.settings_terms:
                String termUrl = "http://www.example.com";
                Intent termIntent = new Intent(Intent.ACTION_VIEW);
                termIntent.setData(Uri.parse(termUrl));
                startActivity(termIntent);
                break;
        }

    }

    @Override
    public void onCreateClicked(String newPath) {
        mPref.edit().putString(getString(R.string.pref_path), newPath).apply();
        tvPath.setText(newPath);
    }
}
