package com.dgtprm.nkt10.musicfinderbyrd.util;

public class SecondsToMinutesConverter {

    public static String convert(int seconds){
        int min = seconds / 60;
        int sec = seconds - (min * 60);
        String secString = sec < 10 ? ("0" + sec) : Integer.toString(sec);

        return min + ":" + secString;
    }
}
