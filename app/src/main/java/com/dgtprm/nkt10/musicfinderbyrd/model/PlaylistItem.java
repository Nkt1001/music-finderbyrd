package com.dgtprm.nkt10.musicfinderbyrd.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PlaylistItem implements Parcelable {
    private static final String TAG = App.TAG + "." + PlaylistItem.class.getSimpleName();

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("tableName")
    @Expose
    private String tableName;

    @SerializedName("count")
    @Expose
    private int count;

    private List<MusicData.Item> musicList;

    public PlaylistItem(String name){
        this.name = name;
        this.tableName = generateTableName(name);
    }

    protected PlaylistItem(Parcel in) {
        name = in.readString();
        tableName = in.readString();
        count = in.readInt();
        musicList = in.createTypedArrayList(MusicData.Item.CREATOR);
    }

    public static final Creator<PlaylistItem> CREATOR = new Creator<PlaylistItem>() {
        @Override
        public PlaylistItem createFromParcel(Parcel in) {
            return new PlaylistItem(in);
        }

        @Override
        public PlaylistItem[] newArray(int size) {
            return new PlaylistItem[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.tableName = generateTableName(name);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static String generateTableName(String name){
        return "playlist"+name.hashCode();
    }

    public String getTableName() {
        return tableName;
    }


    public List<MusicData.Item> getMusicList() {

        return musicList != null ? musicList : new ArrayList<MusicData.Item>();
    }

    public void setMusicList(List<MusicData.Item> musicList) {
        this.musicList = new ArrayList<>(musicList);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlaylistItem)) return false;

        PlaylistItem that = (PlaylistItem) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return !(tableName != null ? !tableName.equals(that.tableName) : that.tableName != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (tableName != null ? tableName.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(tableName);
        dest.writeInt(count);
        dest.writeTypedList(musicList);
    }
}
