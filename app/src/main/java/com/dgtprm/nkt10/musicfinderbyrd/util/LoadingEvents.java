package com.dgtprm.nkt10.musicfinderbyrd.util;

public enum LoadingEvents {
    DOWNLOADED_NEW, LOADING_NEW, REMOVE_DOWNLOADED, LOADING_FAILED,
    PROGRESS_CHANGED, LOADING_FINISHED
}
