package com.dgtprm.nkt10.musicfinderbyrd;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicItemStateHolder;
import com.dgtprm.nkt10.musicfinderbyrd.sql.DBMusicManager;
import com.dgtprm.nkt10.musicfinderbyrd.sql.PlaylistInfoProvider;
import com.dgtprm.nkt10.musicfinderbyrd.sql.SavedAudioReaderContract.PlaylistReaderEntry;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;
import com.vk.sdk.util.VKUtil;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class App extends Application {

    public static final String TAG = App.class.getSimpleName();

    private SharedPreferences mPref;

    public static final String PLAYLIST_SPLIT_EXPRESSION = "&";

    private static long userId=0;

    private static String mPath="";

    private List<MusicData.Item> mPlaylist=null;

    private Tracker mTracker;

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if(newToken == null){
                Log.d(TAG, "onVKAccessTokenChanged: new token null");
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mPref = PreferenceManager.getDefaultSharedPreferences(this);

        initPlaylist();
        DownloadedListHolder.initDownloadedList(this, mPlaylist);

        initLastPlayingPlaylist();

        vkAccessTokenTracker.startTracking();

        Fabric.with(this);
    }

    private void initLastPlayingPlaylist() {
        int itemPlayingID = mPref.getInt(getString(R.string.pref_item_playing_id), -1);
        MusicData.Item itemPlaying = new MusicData.Item(itemPlayingID, MusicItemStateHolder.State.STATE_DEFAULT);
        Log.d(TAG, "initPlaylistHolder: " + itemPlaying.getId());
        boolean foundLastPlaying = false;

//        String baseUri = PlaylistInfoProvider.PLAYLIST_CONTENT_URI.toString() + PlaylistInfoProvider.BASE_PLAYLIST_AUDIO_PATH;
        Cursor cursor = getContentResolver().query(PlaylistInfoProvider.MY_PLAYLIST_CONTENT_URI, null, null, null,null);

        if (cursor != null){
            if (cursor.moveToFirst()){
                ArrayList<MusicData.Item> basePlaylist = new ArrayList<>();

                do {
                    MusicData.Item item = getItem(cursor, MusicItemStateHolder.State.STATE_DEFAULT);
                    basePlaylist.add(item);

                    if (!foundLastPlaying){
                        if (item.equals(itemPlaying)) {
                            foundLastPlaying = true;
                            itemPlaying = new MusicData.Item(item);
                            Log.d(TAG, "initPlaylistHolder: " + itemPlaying);
                        }
                    }
                } while (cursor.moveToNext());

                PlaylistHolder.newInstance(basePlaylist).setLastPlaying(itemPlaying);
            }
            cursor.close();
        }
    }


    private void initPlaylist(){
        mPlaylist = null;
        List<MusicData.Item> deleteList = new ArrayList<>();
        Cursor cursor = getContentResolver().query(PlaylistInfoProvider.PLAYLIST_CONTENT_URI, null, null, null, null);

        if (cursor != null) {
            mPlaylist = new ArrayList<>();

            if (cursor.moveToFirst()) {
                Log.d(TAG, "initPlaylist: cursor not empty");
                do {
                    MusicData.Item item = getItem(cursor, MusicItemStateHolder.State.STATE_DOWNLOADED);


                    if (new File(item.getUri()).exists()) {
                        mPlaylist.add(0, item);
                    } else {
                        Log.d(TAG, "initPlaylist: item " + item + " not found and will be removed");
                        deleteList.add(item);
                    }
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        }

        if (deleteList.size() > 0)
            deleteFromDB(deleteList);
    }

    @NonNull
    public MusicData.Item getItem(Cursor cursor, MusicItemStateHolder.State state) {
        MusicData.Item item = new MusicData.Item(cursor.getInt(cursor.getColumnIndex(PlaylistReaderEntry.DB_AUDIO_ID)), state);
        item.setUrl(cursor.getString(cursor.getColumnIndex(PlaylistReaderEntry.DB_PATH_URL)));
        item.setUri(cursor.getString(cursor.getColumnIndex(PlaylistReaderEntry.DB_PATH_URI)));
        item.setArtist(cursor.getString(cursor.getColumnIndex(PlaylistReaderEntry.DB_ARTIST)));
        item.setTitle(cursor.getString(cursor.getColumnIndex(PlaylistReaderEntry.DB_NAME)));
        item.setDuration(cursor.getInt(cursor.getColumnIndex(PlaylistReaderEntry.DB_DURATION)));
        int i = cursor.getInt(cursor.getColumnIndex(PlaylistReaderEntry._ID));
        Log.d(TAG, "getItem: autoincrement id = " + i);
        item.set_id(i);
        return item;
    }

    public List<MusicData.Item> getItemList(Cursor cursor, MusicItemStateHolder.State state){
        List<MusicData.Item> itemsList = new ArrayList<>();
        if (cursor != null){
            if (cursor.moveToFirst()){
                do {
                    itemsList.add(getItem(cursor, state));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return itemsList;
    }

//    private void managePlaylist(String pl, MusicData.Item item){
//
//        String[] playlistTitles = pl.split(PLAYLIST_SPLIT_EXPRESSION);
//        for (String s : playlistTitles) {
//            PlaylistItem plItem = PlaylistItem.createNewOrGetExist(s);
//
//            plItem.getElements().add(item);
//        }
//
//    }

    private void deleteFromDB(List<MusicData.Item> list){
        new DBMusicManager(this).deleteList(PlaylistInfoProvider.PLAYLIST_CONTENT_URI, list);
    }

    public static long getUserId() {
        return userId;
    }

    public static void setUserId(long userId) {
        App.userId = userId;
    }

    public void initDataPath(SharedPreferences preferences){
        Log.d(TAG, "initDataPath: ");
        mPath = preferences.getString(getString(R.string.pref_path), "");

        if(mPath.isEmpty()){
            File file = getDiskCacheDir(getApplicationContext(), "vkMusic");
            if (createDirIfNotExtists(file.getPath())) {

                preferences.edit().putString(getString(R.string.pref_path), file.getPath()).apply();
            }
        } else{
            createDirIfNotExtists(mPath);
        }
    }

    public File getDiskCacheDir(Context context, String uniqueName){
        final String cachePath =
                Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                        !Environment.isExternalStorageRemovable() ? Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getPath() :
                        context.getFilesDir().getPath();
        Log.d(TAG, "getDiskCacheDir: " + cachePath + " uniquename = " + uniqueName);

        return new File(cachePath + File.separator + uniqueName);
    }

    public boolean createDirIfNotExtists(String path){
        Log.d(TAG, "createDirIfNotExtists() called with: " + "path = [" + path + "]");
//        boolean res = true;

        File file = new File(path);
        if(!file.exists()){
            if(!file.mkdirs()){
                Log.d(TAG, "createDirIfNotExtists: Impossible create direrctory");
                return false;
            }
        }
        mPath = path;

        return true;
    }

    public static String getmPath() {
        return mPath;
    }

    public void initialize(int id){

        Log.d(TAG, "initialize: " + Arrays.toString(VKUtil.getCertificateFingerprint(this, this.getPackageName())));
        try {
            Method initialize = VKSdk.class.getDeclaredMethod("initialize", Context.class, int.class, String.class);
            initialize.setAccessible(true);

            initialize.invoke(VKSdk.class, this, id, "");

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
