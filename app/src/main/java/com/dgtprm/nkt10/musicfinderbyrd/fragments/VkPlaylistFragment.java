package com.dgtprm.nkt10.musicfinderbyrd.fragments;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.adapters.VkPlaylistAdapter;
import com.dgtprm.nkt10.musicfinderbyrd.model.GlobalBanList;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class VkPlaylistFragment extends BasePlaylistFragment {

    private static final String TAG = App.TAG + "." + VkPlaylistFragment.class.getSimpleName();

    public static final int COUNT_ITEMS_ON_PAGE = 30;

    public static final int ARG_AUDIO_GET = 0;
    public static final int ARG_AUDIO_POPULAR = 1;
    public static final int ARG_AUDIO_RECOM = 2;
    public static final int ARG_AUDIO_SEARCH = 3;

    private int currentPage;
    private int countPages;
    private int countItems;
    private volatile boolean isLoading;

    private VkPlaylistAdapter mAdapter;

    private String name;

    private VKRequest.VKRequestListener requestListener = new VKRequest.VKRequestListener() {
        @Override
        public void onComplete(VKResponse response) {


            turnOffProgress();
            String responseString = response.responseString;
            Log.d(TAG, "onComplete: " + responseString);

            if(getArguments().getInt(ARG_METHOD) != ARG_AUDIO_POPULAR){
                MusicData data = new Gson().fromJson(response.responseString, MusicData.class);

                countItems = data.getResponse().getCount();

                myData = data.getResponse().getItems();
            } else{

                countItems = 1000;

                String responseArray = MusicData.parseJson(responseString);
                Type mType = new TypeToken<List<MusicData.Item>>(){}.getType();
                myData = new Gson().fromJson(responseArray, mType);
            }

            checkForBanned(myData);

            countPages = countItems / COUNT_ITEMS_ON_PAGE;
            PlaylistHolder playlistHolder = PlaylistHolder.getInstance();

            if(playlistHolder == null)
                PlaylistHolder.newInstance(new ArrayList<MusicData.Item>());

            checkForDownloading(myData);
            checkForDownloaded(myData);

            if (recyclerView != null) {
                if (null != mAdapter)
                    mAdapter.addNewPage(myData);
                else {
                    mAdapter = new VkPlaylistAdapter(myData, mListener);
                    recyclerView.setAdapter(mAdapter);
                }
            }

            isLoading = false;
            super.onComplete(response);
        }

        @Override
        public void onError(VKError error) {
            turnOffProgress();
            Toast.makeText(getActivity(), "Attempt failed. Refresh page", Toast.LENGTH_SHORT).show();
            super.onError(error);
        }

    };


    public VkPlaylistFragment(){
    }

    public static VkPlaylistFragment newInstance(Bundle requestArgs) {

        Bundle args = new Bundle();

        args.putAll(requestArgs);
        VkPlaylistFragment fragment = new VkPlaylistFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mPref.getBoolean(getString(R.string.pref_allow_api), true))
            loadNewData(getArguments());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        setScrollListener((RecyclerView) view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void setmAdapter() {
        if (mAdapter == null) {
            mAdapter = new VkPlaylistAdapter(myData, mListener);
        }
        recyclerView.setAdapter(mAdapter);
    }

    private void setScrollListener(RecyclerView recyclerView){

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                switch (newState) {
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        Log.d(TAG, "onScrollStateChanged: RecyclerView.SCROLL_STATE_DRAGGING");
                        break;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                    Log.d(TAG, "onScrolled: ");
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    float totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    float lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();

                    float visiblePercent = 100 * lastPosition / totalItemCount;


                    if ((totalItemCount - lastPosition) < 3 && countPages > currentPage && !isLoading) {
                        Log.d(TAG, "onScrolled: total item count = " + totalItemCount);
                        Log.d(TAG, "onScrolled: lastPosition = " + lastPosition);
                        Log.d(TAG, "onScrolled: visiblePercent = " + visiblePercent);

                        isLoading = true;
                        loadNextPage();
                    }
                }
            }
        });
    }

    public void refreshList(){
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    private void loadNextPage(){
        int offset = (currentPage+1) * COUNT_ITEMS_ON_PAGE;
        ++currentPage;

        String[] params = getArguments().getStringArray(ARG_PARAMS);
        int method = getArguments().getInt(ARG_METHOD);

        if (params != null) {
            for (int i = 0; i < params.length; i++) {

                if (params[i].equals("offset")) {
                    params[i + 1] = String.valueOf(offset);
                    break;
                }

            }
        }

        Bundle b = new Bundle();
        b.putStringArray(ARG_PARAMS, params);
        b.putInt(ARG_METHOD, method);

        loadNewData(b);
    }

    private void loadNewData(final Bundle args) {
        turnOnProgress();

        String[] params = args.getStringArray(ARG_PARAMS);
        VKRequest request = getRequest(args.getInt(ARG_METHOD));

        if (request != null && params != null) {
            request.addExtraParameters(VKParameters.from(params));
            request.executeWithListener(requestListener);
        }
    }

    private VKRequest getRequest(int type){

        setName(type);

        switch (type){
            case ARG_AUDIO_GET:    return VKApi.audio().get();
            case ARG_AUDIO_POPULAR:    return VKApi.audio().getPopular();
            case ARG_AUDIO_RECOM:    return VKApi.audio().getRecommendations();
            case ARG_AUDIO_SEARCH: return VKApi.audio().search(null);
            default:   return null;
        }
    }

    private void setName(int type) {
        switch (type){
            case ARG_AUDIO_GET:     name = "All Music Fragment"; break;
            case ARG_AUDIO_POPULAR:    name = "Popular Music Fragment"; break;
            case ARG_AUDIO_RECOM:    name = "Recommended Music Fragment"; break;
            case ARG_AUDIO_SEARCH: name = "Searching Fragment"; break;
        }
    }


    private void checkForBanned(List<MusicData.Item> data){
        GlobalBanList banList = GlobalBanList.getInstance();

        if (banList == null || banList.getBanIds().size() == 0) return;

        for (MusicData.Item item : data){
            if (banList.getBanIds().contains(item.getId()))
                data.remove(item);
        }
    }
}
