package com.dgtprm.nkt10.musicfinderbyrd.util;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;

import com.squareup.picasso.Transformation;

/**
 * Created by Администратор on 25.02.2016.
 */
public class CircleTransform implements Transformation {
    @Override
    public Bitmap transform(Bitmap source) {

        int size = source.getWidth();

        Bitmap squaredBmp = Bitmap.createBitmap(source);

        if(squaredBmp != source){
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBmp, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        squaredBmp.recycle();
        return bitmap;
    }

    @Override
    public String key() {
        return "circle";
    }
}
