package com.dgtprm.nkt10.musicfinderbyrd.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.adapters.BaseRecyclerViewAdapter;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicItemStateHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadedListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.DownloadingListHolder;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;
import com.google.android.gms.analytics.Tracker;

import java.util.List;

public abstract class BasePlaylistFragment extends Fragment {

    private static final String TAG = App.TAG + "." + BasePlaylistFragment.class.getSimpleName();

    protected SharedPreferences mPref;

    protected RecyclerView recyclerView;
    protected ProgressBar progressBar;

    protected List<MusicData.Item> myData;

    private BaseRecyclerViewAdapter adapter;
    protected PlaylistFragmentInteractionListener mListener;


    public static final String ARG_METHOD = "ARG_METHOD";
    public static final String ARG_PARAMS = "ARG_PARAMS";

    protected Tracker mTracker;


    public BasePlaylistFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myData = null;
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        progressBar = (ProgressBar) getActivity().findViewById(R.id.main_progressBar);

        App app = (App) getActivity().getApplication();
        mTracker = app.getDefaultTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_playlistitem_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;

            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            DefaultItemAnimator animator = new DefaultItemAnimator(){
                @Override
                public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                    return true;
                }
            };
            recyclerView.setItemAnimator(animator);

            if(myData != null) setmAdapter();
        }

        return view;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach() called with: " + "context = [" + activity + "]");
        if (activity instanceof PlaylistFragmentInteractionListener) {
            mListener = (PlaylistFragmentInteractionListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    public abstract void setmAdapter();

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        turnOffProgress();
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.d(TAG, "onStop: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy: ");

        if(PlaylistHolder.getInstance() != null) PlaylistHolder.getInstance().deleteObservers();
        if(DownloadedListHolder.getInstance() != null) DownloadedListHolder.getInstance().deleteObservers();
        if(DownloadingListHolder.getInstance() != null) DownloadingListHolder.getInstance().deleteObservers();
    }

    protected void checkForDownloading(List<MusicData.Item> data){
        List<MusicData.Item> downloadingList = DownloadingListHolder.getInstance().getDownloadingList();

        for (MusicData.Item item : downloadingList){
            if (data.contains(item)){
                int index = data.indexOf(item);
                item.getStateHolder().hideExtraMenu();
                data.set(index, item);
            }
        }
    }

    protected void checkForDownloaded(List<MusicData.Item> data) {
        List<MusicData.Item> downloadedList = DownloadedListHolder.getInstance().getDownloadedList();

        for (int i = 0; i < downloadedList.size(); i++){

            MusicData.Item currentItem = downloadedList.get(i);
            if (data.contains(currentItem)){

                int index = data.indexOf(currentItem);
                for (int j = index; j < data.size(); j++){
                    if (data.get(j).equals(currentItem)) {
                        MusicData.Item downloadedItem = data.get(j);
                        downloadedItem.setStateHolder(MusicItemStateHolder.State.STATE_DOWNLOADED);
                        downloadedItem.setUri(currentItem.getUri());
                    }
                }
            }
        }
    }

    protected void turnOnProgress(){
        if (progressBar == null) return;

        progressBar.setVisibility(View.VISIBLE);
    }

    protected void turnOffProgress(){
        progressBar.setVisibility(View.GONE);
    }

    public interface PlaylistFragmentInteractionListener {
        // TODO: Update argument type and name
        void onPlayButtonPressed(MusicData.Item item, List<MusicData.Item> items, boolean inCustomPlaylist);
        void onExtraButtonPressed(BaseRecyclerViewAdapter.ViewHolder viewHolder);
        void onDownloadRemoveButtonPressed(MusicData.Item item, Uri uriTable, boolean onlyRemove);
    }
}
