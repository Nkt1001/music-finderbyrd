package com.dgtprm.nkt10.musicfinderbyrd.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.util.SecondsToMinutesConverter;
import com.google.android.gms.analytics.Tracker;

public class MusicInfoDialog extends DialogFragment {

    MusicData.Item item;

    protected Tracker mTracker;

    public static MusicInfoDialog newInstance(Bundle args) {

        MusicInfoDialog fragment = new MusicInfoDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        item = getArguments().getParcelable(getString(R.string.extra_info));

        App app = (App) getActivity().getApplication();
        mTracker = app.getDefaultTracker();
        mTracker.setScreenName("MusicInfoDialog");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout v = (LinearLayout) inflater.inflate(R.layout.layout_info_dialog, container, false);

        return adjustDialog(inflater, v);
    }

    private LinearLayout adjustDialog(LayoutInflater inflater, LinearLayout baseLayout){

        TextView title;
        TextView value;

        String[] dialogTitles = getResources().getStringArray(R.array.dialog_titles);

        for(int i = 0; i < dialogTitles.length; i++){
            View item = inflater.inflate(R.layout.layout_info_dialog_track, baseLayout, false);

            String stringTitle = dialogTitles[i];
            if(i == 0){
                item.findViewById(R.id.dialog_info_icon).setVisibility(View.VISIBLE);
            }
            title = (TextView) item.findViewById(R.id.dialog_item_title);
            value = (TextView) item.findViewById(R.id.dialog_item_value);

            title.setText(stringTitle);
            value.setText(getMyValue(stringTitle));

            baseLayout.addView(item);
        }

        return baseLayout;
    }

    private String getMyValue(String stringTitle){

        String value = "";
        if (item == null) return value;

        if(stringTitle.equals(getString(R.string.dialog_track)))
            value = item.getTitle();
        else if(stringTitle.equals(getString(R.string.dialog_artist)))
            value =item.getArtist();
        else if(stringTitle.equals(getString(R.string.dialog_duration)))
            value =SecondsToMinutesConverter.convert(item.getDuration());
        else if(stringTitle.equals(getString(R.string.dialog_id)))
            value = item.getId().toString();

        return value;
    }
}
