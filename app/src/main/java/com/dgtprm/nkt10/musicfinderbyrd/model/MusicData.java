package com.dgtprm.nkt10.musicfinderbyrd.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MusicData {

    private static final String TAG = App.TAG + "." + MusicData.class.getSimpleName();

    @SerializedName("response")
    @Expose
    private Response response;


    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public static String parseJson(String json) {

        JSONArray respArray = null;
        try {
            respArray = new JSONObject(json).getJSONArray("response");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return respArray != null ? respArray.toString() : null;
    }

    public static class Response {

        @SerializedName("count")
        @Expose
        private Integer count;
        @SerializedName("items")
        @Expose
        private List<Item> items = new ArrayList<>();

        protected Response(Parcel in) {
            count = in.readInt();
            items = in.createTypedArrayList(Item.CREATOR);
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

    }

    public static class Item implements Parcelable {

        @SerializedName("_id")
        @Expose
        private int _id;

        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("artist")
        @Expose
        private String artist;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("duration")
        @Expose
        private Integer duration;

        @SerializedName("url")
        @Expose
        private String url;

        private String uri;

        private MusicItemStateHolder stateHolder;

        public Item(int id, MusicItemStateHolder.State state){
            this.id = id;
            stateHolder = new MusicItemStateHolder(state);
        }

        public Item(Item from){
            this.id = from.id;
            this._id = from._id;
            this.artist = from.artist;
            this.title = from.title;
            this.duration = from.duration;
            this.url = from.url;
            this.uri = from.uri;
            this.stateHolder = from.stateHolder;
        }

        private Item(){
            stateHolder = new MusicItemStateHolder(MusicItemStateHolder.State.STATE_DEFAULT);
        }

        protected Item(Parcel in) {
            artist = in.readString();
            title = in.readString();
            url = in.readString();
            duration = in.readInt();
            id = in.readInt();
            _id = in.readInt();
            stateHolder = in.readParcelable(MusicItemStateHolder.class.getClassLoader());
            uri = in.readString();
        }

        public static final Creator<Item> CREATOR = new Creator<Item>() {
            @Override
            public Item createFromParcel(Parcel in) {
                return new Item(in);
            }

            @Override
            public Item[] newArray(int size) {
                return new Item[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getArtist() {
            return artist;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getDuration() {
            return duration;
        }

        public void setDuration(Integer duration) {
            this.duration = duration;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int get_id() {
            return _id;
        }

        public void set_id(int _id) {
            this._id = _id;
        }

        public MusicItemStateHolder getStateHolder() {
            return stateHolder;
        }

        public void setStateHolder(MusicItemStateHolder.State state) {
            this.stateHolder = new MusicItemStateHolder(state);
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Item)) return false;

            Item item = (Item) o;

            return !(id != null ? !id.equals(item.id) : item.id != null);

        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }

        @Override
        public String toString() {
            return "[ id = " + id + "; title = " + title + "]";
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(artist);
            dest.writeString(title);
            dest.writeString(url);
            dest.writeInt(duration);
            dest.writeInt(id);
            dest.writeInt(_id);
            dest.writeParcelable(stateHolder, flags);
            dest.writeString(uri);
        }
    }
}
