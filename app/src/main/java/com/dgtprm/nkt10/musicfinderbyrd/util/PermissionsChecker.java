package com.dgtprm.nkt10.musicfinderbyrd.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.dgtprm.nkt10.musicfinderbyrd.App;

public class PermissionsChecker {
    private static final String TAG = App.TAG + "." + PermissionsChecker.class.getSimpleName();
    private final Context context;

    public PermissionsChecker(Context context){
        this.context = context;
    }

    public boolean lacksPermissions(String... permissions){
        for (String permission : permissions){
            if (lacksPermission(permission)) {
                Log.d(TAG, "lacksPermissions: " + permission);
                return true;
            }
        }
        return false;
    }

    private boolean lacksPermission(String permission){
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_DENIED;
    }
}
