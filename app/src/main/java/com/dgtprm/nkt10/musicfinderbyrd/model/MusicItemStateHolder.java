package com.dgtprm.nkt10.musicfinderbyrd.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import com.dgtprm.nkt10.musicfinderbyrd.R;

public class MusicItemStateHolder implements Parcelable {

    public enum State {STATE_DEFAULT, STATE_DOWNLOADED, STATE_DOWNLOADING, STATE_PLAYLIST_ITEM}

    private State state;

    private Integer extraMenuVisibility;

    private Integer extraMenuIconRes;

    private Integer stickerVisibility;

    private Integer progressBarVisible;

    private Integer stickerRes;

    private Integer loadingButtonRes;

    private Integer progressValue;

    public MusicItemStateHolder(State state){
        this.state = state;

        switch (state){
            case STATE_DEFAULT:
                initDefault();
                break;
            case STATE_DOWNLOADING:
                initDownloading();
                break;
            case STATE_DOWNLOADED:
                initDownloaded();
                break;
            case STATE_PLAYLIST_ITEM:
                initPlaylistItem();
                break;
        }

    }

    private void initDownloading() {
        init();
        stickerVisibility = View.VISIBLE;
        stickerRes = getStickerLoading();
        loadingButtonRes = R.drawable.playlist_item_extra_download_selector;
    }

    private void init(){
        hideExtraMenu();
        progressBarVisible = View.GONE;
        progressValue = 0;
        stickerRes = R.drawable.ic_dl_status;
    }

    private void initDefault(){
        init();
        stickerVisibility = View.INVISIBLE;
        loadingButtonRes = R.drawable.playlist_item_extra_download_selector;
    }

    private void initPlaylistItem(){
        init();
        stickerVisibility = View.INVISIBLE;
        loadingButtonRes = R.drawable.delete_button_selector;
    }

    private void initDownloaded(){
        init();
        stickerVisibility = View.VISIBLE;
        loadingButtonRes = R.drawable.delete_button_selector;
    }

    public int getStickerLoading(){
        return R.drawable.ic_dl_status_loading;
    }

    public int getStickerDownloaded(){
        return R.drawable.ic_dl_status;
    }

    public int getDownloadSelectorDrawableRes(){
        return R.drawable.playlist_item_extra_download_selector;
    }

    public int getDeleteSelectorDrawable(){
        return R.drawable.delete_button_selector;
    }

    public void hideExtraMenu(){
        extraMenuVisibility = View.GONE;
        extraMenuIconRes = R.drawable.ic_more_vert;
    }

    public void showExtraMenu(){
        extraMenuVisibility = View.VISIBLE;
        extraMenuIconRes = R.drawable.ic_more_hor_24px;
    }

    public Integer getExtraMenuVisibility() {
        return extraMenuVisibility;
    }

    public void setExtraMenuVisibility(Integer extraMenuVisibility) {
        this.extraMenuVisibility = extraMenuVisibility;
    }

    public Integer getExtraMenuIconRes() {
        return extraMenuIconRes;
    }

    public void setExtraMenuIconRes(Integer extraMenuIconRes) {
        this.extraMenuIconRes = extraMenuIconRes;
    }

    public Integer getStickerVisibility() {
        return stickerVisibility;
    }

    public void setStickerVisibility(boolean stickerVisibility) {
        this.stickerVisibility = stickerVisibility ? View.VISIBLE : View.INVISIBLE;
    }

    public Integer getProgressBarVisible() {
        return progressBarVisible;
    }

    public void setProgressBarVisible(Integer progressBarVisible) {
        this.progressBarVisible = progressBarVisible;
    }

    public Integer getStickerRes() {
        return stickerRes;
    }

    public void setStickerRes(Integer stickerRes) {
        this.stickerRes = stickerRes;
    }

    public Integer getLoadingButtonRes() {
        return loadingButtonRes;
    }

    public void setLoadingButtonRes(Integer loadingButtonRes) {
        this.loadingButtonRes = loadingButtonRes;
    }

    public Integer getProgressValue() {
        return progressValue;
    }

    public void setProgressValue(Integer progressValue) {
        this.progressValue = progressValue;
    }

    protected MusicItemStateHolder(Parcel in) {
        extraMenuVisibility = in.readInt();
        extraMenuIconRes = in.readInt();
        stickerVisibility = in.readInt();
        progressBarVisible = in.readInt();
        progressValue = in.readInt();
        stickerRes = in.readInt();
        loadingButtonRes = in.readInt();
    }

    public static final Creator<MusicItemStateHolder> CREATOR = new Creator<MusicItemStateHolder>() {
        @Override
        public MusicItemStateHolder createFromParcel(Parcel in) {
            return new MusicItemStateHolder(in);
        }

        @Override
        public MusicItemStateHolder[] newArray(int size) {
            return new MusicItemStateHolder[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(extraMenuVisibility);
        dest.writeInt(extraMenuIconRes);
        dest.writeInt(stickerVisibility);
        dest.writeInt(progressBarVisible);
        dest.writeInt(progressValue);
        dest.writeInt(stickerRes);
        dest.writeInt(loadingButtonRes);
    }

    @Override
    public String toString() {
        return "State = " + state + "; " + "extra visibility gone " + (extraMenuVisibility == View.GONE);
    }
}
