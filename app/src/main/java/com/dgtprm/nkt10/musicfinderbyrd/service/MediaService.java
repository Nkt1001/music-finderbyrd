package com.dgtprm.nkt10.musicfinderbyrd.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.dgtprm.nkt10.musicfinderbyrd.App;
import com.dgtprm.nkt10.musicfinderbyrd.MainActivity;
import com.dgtprm.nkt10.musicfinderbyrd.PlayerActivity;
import com.dgtprm.nkt10.musicfinderbyrd.R;
import com.dgtprm.nkt10.musicfinderbyrd.model.MusicData;
import com.dgtprm.nkt10.musicfinderbyrd.sql.DBMusicManager;
import com.dgtprm.nkt10.musicfinderbyrd.sql.PlaylistInfoProvider;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlayerEvents;
import com.dgtprm.nkt10.musicfinderbyrd.util.player.PlaylistHolder;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class MediaService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    public static final String COMMAND = "COMMAND";
    public static final String ITEM = "ITEM";

    private Notification notification;

    public static final String PROGRESS_PERCENTS = "PROGRESS_PERCENTS";
    public static final String PROGRESS_MILLIS = "PROGRESS_VALUE";
    public static final String EXTRA_IS_PLAYING = "EXTRA_IS_PLAYING";
    public static final String EXTRA_IS_PAUSED = "EXTRA_IS_PAUSED";

    private static final String TAG = App.TAG + "." + MediaService.class.getSimpleName();

    private MediaPlayer mediaPlayer = null;
    private Binder mBinder = new MyBinder();

    private boolean isLooping;

    private final int SEEK_TIME_FORWARD = 2000;
    private final int SEEK_TIME_BACKWARD = 3000;

    private Timer timer;
    private Timer bakingTimer;
    private boolean isPaused = false;
    private boolean isLoading = false;
    private volatile boolean bakingTimerFlag = false;

    private WifiManager.WifiLock wifiLock;
    private SharedPreferences mPref;

    public MediaService() {
    }

    public static void startMediaService(Context context, MusicData.Item item) {
        Intent intent = new Intent(context, MediaService.class);
        intent.putExtra(MediaService.COMMAND, PlayerEvents.PLAYER_PLAY);
        intent.putExtra(MediaService.ITEM, item);
        intent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
        context.startService(intent);
    }

    public static void stopMusicKillService(Context context) {
        Intent intent = new Intent(context, MediaService.class);
        intent.putExtra(MediaService.COMMAND, PlayerEvents.PLAYER_STOP);
        intent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        context.startService(intent);
    }

    public static void pauseMediaService(Context context) {
        Intent intent = new Intent(context, MediaService.class);
        intent.putExtra(MediaService.COMMAND, PlayerEvents.PLAYER_PAUSE);
        intent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate: ");

        mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        isLooping = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(getString(R.string.pref_repeat), false);
        setShuffleMode(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(getString(R.string.pref_shuffle), false));
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "onStartCommand: " + intent);
        if (intent != null) {

            if (intent.getExtras() != null) {

                PlayerEvents event = (PlayerEvents) intent.getSerializableExtra(COMMAND);
                MusicData.Item item = intent.getParcelableExtra(ITEM);

                if (event.equals(PlayerEvents.PLAYER_PLAY)) {

                    if (isPaused && item.equals(PlaylistHolder.getInstance().getLastPlaying()))
                        resumeMusic();
                    else
                        playMusic(item);

                } else if (event.equals(PlayerEvents.PLAYER_STOP)) {
                    killService();
                } else if (event.equals(PlayerEvents.PLAYER_PAUSE)) {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        pauseMusic();
                    } else {
                        killService();
                    }
                }
            }

            if (intent.getAction() != null) {

                if (intent.getAction().equals(Constants.ACTION.PREV_ACTION)) {
                    playPrevious();
                } else if (intent.getAction().equals(Constants.ACTION.PLAY_ACTION)) {

                    if (mediaPlayer != null && mediaPlayer.isPlaying())
                        pauseMusic();
                    else
                        resumeMusic();

                } else if (intent.getAction().equals(Constants.ACTION.NEXT_ACTION)) {
                    playNext();
                }
            }
        }

        Intent response = new Intent(PlayerActivity.ACTION_MUSIC).putExtra(PlayerActivity.SERVICE_COMMAND, PlayerEvents.PLAYER_PREPARING);

        if (mediaPlayer != null)
            response.putExtra(EXTRA_IS_PLAYING, mediaPlayer.isPlaying());
        else response.putExtra(EXTRA_IS_PLAYING, false);

        sendBroadcast(response);

        return START_NOT_STICKY;
    }

    private void killService() {
        stopForeground(true);
        stopMusic();
        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Intent response = new Intent(PlayerActivity.ACTION_MUSIC).putExtra(PlayerActivity.SERVICE_COMMAND, PlayerEvents.PLAYER_PREPARING)
                .putExtra(EXTRA_IS_PAUSED, isPaused);

        if (PlaylistHolder.getInstance().getItemPlaying() != null){
            Log.d(TAG, "onBind: item");
            response.putExtra(ITEM, PlaylistHolder.getInstance().getItemPlaying());
        } else if (PlaylistHolder.getInstance().getLastPlaying() != null){
            Log.d(TAG, "onBind: last");
            response.putExtra(ITEM, PlaylistHolder.getInstance().getLastPlaying());
        }

        sendBroadcast(response);
        setTimer();
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        Intent response = new Intent(PlayerActivity.ACTION_MUSIC).putExtra(PlayerActivity.SERVICE_COMMAND, PlayerEvents.PLAYER_PREPARING)
                .putExtra(EXTRA_IS_PAUSED, isPaused);

        if (PlaylistHolder.getInstance().getItemPlaying() != null){
            Log.d(TAG, "onReBind: item");
            response.putExtra(ITEM, PlaylistHolder.getInstance().getItemPlaying());
        } else if (PlaylistHolder.getInstance().getLastPlaying() != null){
            Log.d(TAG, "onReBind: last");
            response.putExtra(ITEM, PlaylistHolder.getInstance().getLastPlaying());
        }

        sendBroadcast(response);
        setTimer();

        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        finishTimer();
        Log.d(TAG, "onUnbind: ");
        return true;
    }

    private void finishTimer(){
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    private void setTimer(){
        finishTimer();

        timer = new Timer();
        TimerTask tTask = new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(PlayerActivity.ACTION_MUSIC);
                intent.putExtra(PlayerActivity.SERVICE_COMMAND, PlayerEvents.PLAYER_TICK)
                        .putExtra(PROGRESS_PERCENTS, getProgressValue()).putExtra(PROGRESS_MILLIS, getCurrentPosition());


                sendBroadcast(intent);
            }
        };
        timer.schedule(tTask, 0, 1000);
    }

    @Override
    public void onDestroy() {

        releaseMP();
        savePlaylist();
        savePlayingItemID(PlaylistHolder.getInstance().getLastPlaying());
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }

    private void savePlaylist() {

        if (PlaylistHolder.getInstance().getPlaylist() != null && PlaylistHolder.getInstance().getPlaylist().size() > 0){

            DBMusicManager manager = new DBMusicManager(getApplicationContext());

            manager.deleteAll(PlaylistInfoProvider.MY_PLAYLIST_CONTENT_URI);

            Log.d(TAG, "savePlaylist: saving playlist");
            for (MusicData.Item item : PlaylistHolder.getInstance().getPlaylist()){
                manager.insert(PlaylistInfoProvider.MY_PLAYLIST_CONTENT_URI, item);
            }
        }
    }

    private void savePlayingItemID(MusicData.Item item){
        Log.d(TAG, "savePlayingItemID: saving item");
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putInt(getString(R.string.pref_item_playing_id), item.getId()).apply();
    }

    public void playMusic(MusicData.Item item) {

        Log.d(TAG, "playMusic: ");

//        releaseMP();

        wifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "mylock");
        wifiLock.acquire();

        isLoading = true;

        if(mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            Log.d(TAG, "playMusic: mediaplayer == null");
        }
        mediaPlayer.reset();

        PlaylistHolder.getInstance().setItemPlaying(item);
        if (item.getUri() == null || item.getUri().isEmpty()) {
            Log.d(TAG, "playMusic: url " + item.getUrl());
            try {
                mediaPlayer.setDataSource(item.getUrl());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            Log.d(TAG, "playMusic: uri " + item.getUri());
            try {
                mediaPlayer.setDataSource(getApplicationContext(), Uri.parse(item.getUri()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        Log.d(TAG, "playMusic: prepare async");

        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);

        mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mediaPlayer.prepareAsync();

        showNotification(item.getArtist(), item.getTitle(), false);

        Intent response = new Intent(PlayerActivity.ACTION_MUSIC).putExtra(PlayerActivity.SERVICE_COMMAND, PlayerEvents.PLAYER_PREPARING)
                .putExtra(ITEM, item).putExtra(EXTRA_IS_PAUSED, isPaused);
        sendBroadcast(response);
    }

    public void pauseMusic(){

        if(mediaPlayer == null) return;

        if(mediaPlayer.isPlaying() && !isLoading) {
            mediaPlayer.pause();
            isPaused = true;
            wifiLock.release();
            PlaylistHolder.getInstance().setStopped();
            showNotification(null, null, false);
        }
    }

    public void resumeMusic() {
        MusicData.Item lastPlayingItem = PlaylistHolder.getInstance().getLastPlaying();

        if(mediaPlayer == null) {
            if (lastPlayingItem != null) {
                playMusic(lastPlayingItem);
            }
        } else {
            if (isPaused && !isLoading) {
                PlaylistHolder.getInstance().setItemPlaying(lastPlayingItem);
                mediaPlayer.start();
                isPaused = false;
                showNotification(null, null, true);
                wifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
                        .createWifiLock(WifiManager.WIFI_MODE_FULL, "mylock");
                wifiLock.acquire();
            }
        }
    }

//    private boolean isReady(){
//
//        bakingTimerFlag = false;
//
//        if (bakingTimer != null) {
//            bakingTimer.cancel();
//            bakingTimer.purge();
//        }
//        TimerTask bakingTask = new TimerTask() {
//            @Override
//            public void run() {
//                try {
//                    TimeUnit.MILLISECONDS.sleep(25);
//                    bakingTimerFlag = true;
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                    bakingTimerFlag = false;
//                } finally {
//                    bakingTimer.cancel();
//                    bakingTimer.purge();
//                }
//            }
//        };
//
//        bakingTimer = new Timer();
//        bakingTimer.schedule(bakingTask, 25);
//
//        return bakingTimerFlag;
//    }

    public void stopMusic(){
        if(mediaPlayer == null) return;

        if(mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }

        if (wifiLock.isHeld())
            wifiLock.release();

        Intent response = new Intent(PlayerActivity.ACTION_MUSIC).putExtra(PlayerActivity.SERVICE_COMMAND, PlayerEvents.PLAYER_STOP);
        sendBroadcast(response);

        PlaylistHolder.getInstance().setStopped();
    }

    public void releaseMP(){
        Log.d(TAG, "releaseMP: ");
        if(null != mediaPlayer){
            try{
                finishTimer();
                mediaPlayer.release();
                mediaPlayer = null;

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public synchronized void seekForward(){
        if (mediaPlayer != null && mediaPlayer.isPlaying()){
            int duration = mediaPlayer.getDuration();
            int position = mediaPlayer.getCurrentPosition() + SEEK_TIME_FORWARD;
            if (position < duration){
                mediaPlayer.seekTo(position);
            }else {
                mediaPlayer.seekTo(duration);
            }
        }
    }

    public synchronized void seekBack() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            int position = mediaPlayer.getCurrentPosition() - SEEK_TIME_BACKWARD;
            if (position > 0) {
                mediaPlayer.seekTo(position);
            } else {
                mediaPlayer.seekTo(0);
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "onPrepared: ");

        mp.start();

        isLoading = false;

        showNotification(null, null, true);

        Intent response = new Intent(PlayerActivity.ACTION_MUSIC);
        response.putExtra(PlayerActivity.SERVICE_COMMAND, PlayerEvents.PLAYER_PREPARED);
        sendBroadcast(response);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
//        logMusicInfo();
        Log.d(TAG, "onCompletion: ");

        if (!isLooping) {
            playNext();
        }
        else {
            playMusic(PlaylistHolder.getInstance().getItemPlaying());
        }
    }

    public void setShuffleMode(boolean shuffle){
        if (PlaylistHolder.getInstance() != null)
            PlaylistHolder.getInstance().setShuffleMode(shuffle);
    }

    public void setRepeatMode(boolean repeat){
        isLooping = repeat;
    }

    public boolean playNext(){
        Log.d(TAG, "playNext: ");
        PlaylistHolder holder = PlaylistHolder.getInstance();

        if (holder == null || holder.getPlaylist().size() == 0)
            return false;

        MusicData.Item next;

        if((holder.getCurrentPlaying()+1) < holder.getPlaylist().size()) {
            int i = holder.getCurrentPlaying();
            next = holder.getPlaylist().get(++i);
        } else {
            next = holder.getPlaylist().get(0);
        }

        playMusic(next);

        return true;
    }

    public boolean playPrevious(){
        Log.d(TAG, "playPrevious: ");

        if (getCurrentPosition() > 3000){
            mediaPlayer.seekTo(0);
            return false;
        }

        PlaylistHolder holder = PlaylistHolder.getInstance();

        if (holder == null || holder.getPlaylist().size() == 0)
            return false;

        MusicData.Item prev;
        if(holder.getCurrentPlaying()-1 >= 0) {
            int i = holder.getCurrentPlaying();
            prev = holder.getPlaylist().get(--i);
        } else {
            prev = holder.getPlaylist().get(holder.getPlaylist().size()-1);
        }

        playMusic(prev);

        return true;
    }

    public synchronized float getProgressValue(){
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {

            return 100 * mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration();
        } else if (mediaPlayer != null && isPaused && !isLoading)
            return 100 * mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration();

        return 0;
    }

    public synchronized int getCurrentPosition(){
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            return mediaPlayer.getCurrentPosition();
        else if (mediaPlayer != null && isPaused && !isLoading)
            return mediaPlayer.getCurrentPosition();
        return 0;
    }

    public int getRemainingTime(){
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            return mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition();

        return (1000 * PlaylistHolder.getInstance().getItemPlaying().getDuration());
    }

    private void showNotification(String artist, String song, boolean isPlaying){
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews views = new RemoteViews(getPackageName(),
                R.layout.status_bar);

// showing default album image
        views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
        views.setViewVisibility(R.id.status_bar_album_art, View.GONE);

        boolean isInPlayer = mPref.getBoolean(getString(R.string.pref_inside_player), false);

        Intent notificationIntent;

        if (isInPlayer)
            notificationIntent = new Intent(this, MainActivity.class);
        else
            notificationIntent = new Intent(this, PlayerActivity.class);

        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Intent previousIntent = new Intent(this, MediaService.class);
        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
                previousIntent, 0);

        Intent playIntent = new Intent(this, MediaService.class);
        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, 0);

        Intent nextIntent = new Intent(this, MediaService.class);
        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                nextIntent, 0);


        views.setOnClickPendingIntent(R.id.status_bar_play, pplayIntent);

        views.setOnClickPendingIntent(R.id.status_bar_next, pnextIntent);

        views.setOnClickPendingIntent(R.id.status_bar_previous, ppreviousIntent);

        if (isPlaying) {
            views.setImageViewResource(R.id.status_bar_play,
                    R.drawable.apollo_holo_dark_pause);
        } else {
            views.setImageViewResource(R.id.status_bar_play,
                    R.drawable.apollo_holo_dark_play);
        }

        if (artist != null)
            views.setTextViewText(R.id.status_bar_track_name, song);

        if (song != null)
            views.setTextViewText(R.id.status_bar_artist_name, artist);

        notification = new Notification.Builder(this).build();
        notification.contentView = views;
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.icon = R.mipmap.ic_launcher;
        notification.contentIntent = pendingIntent;
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);
    }



    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    public class MyBinder extends Binder {

        public MediaService getService() {
            return MediaService.this;
        }
    }

    public static class Constants {
        public interface ACTION {
            String MAIN_ACTION = "com.digitalpromo.action.main";
            String PREV_ACTION = "com.digitalpromo.action.prev";
            String PLAY_ACTION = "com.digitalpromo.action.play";
            String NEXT_ACTION = "com.digitalpromo.action.next";
            String STARTFOREGROUND_ACTION = "com.digitalpromo.action.startforeground";
            String STOPFOREGROUND_ACTION = "com.digitalpromo.action.stopforeground";

        }

        public interface NOTIFICATION_ID {
            int FOREGROUND_SERVICE = 101;
        }
    }

}
