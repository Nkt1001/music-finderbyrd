package com.dgtprm.nkt10.musicfinderbyrd.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Администратор on 25.02.2016.
 */
public class GlobalUser implements Parcelable {

    private static GlobalUser globalUser;

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("photo_100")
    @Expose
    private String photo100;


    protected GlobalUser(Parcel in) {
        id = in.readLong();
        firstName = in.readString();
        lastName = in.readString();
        photo100 = in.readString();
    }

    private GlobalUser(){

    }

    public static GlobalUser getInstance(){
        if(globalUser == null)
            globalUser = new GlobalUser();
        return globalUser;
    }

    public static final Creator<GlobalUser> CREATOR = new Creator<GlobalUser>() {
        @Override
        public GlobalUser createFromParcel(Parcel in) {
            return new GlobalUser(in);
        }

        @Override
        public GlobalUser[] newArray(int size) {
            return new GlobalUser[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoto100() {
        return photo100;
    }

    public void setPhoto100(String photo100) {
        this.photo100 = photo100;
    }

    public static String parseUser(String response){
        try {
            JSONArray array = new JSONObject(response).getJSONArray("response");
            return array.get(0).toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(photo100);
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
