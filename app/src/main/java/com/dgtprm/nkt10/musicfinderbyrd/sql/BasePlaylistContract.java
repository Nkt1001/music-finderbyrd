package com.dgtprm.nkt10.musicfinderbyrd.sql;

import android.provider.BaseColumns;

public class BasePlaylistContract {
    public BasePlaylistContract() {
    }

    public static abstract class BasePlaylistEntry implements BaseColumns{
        public static final String TABLE_NAME = "baseplaylist";

        public static final String DB_AUDIO_ID = "trackId";
        public static final String DB_NAME = "name";
        public static final String DB_ARTIST = "artist";
        public static final String DB_DURATION = "duration";
        public static final String DB_PATH_URI = "uri";
        public static final String DB_PATH_URL = "url";
    }
}
